#ifndef __ZMVS_VEHICLE_DISCRETE_GEAR_BOX_DSC_H__
#define __ZMVS_VEHICLE_DISCRETE_GEAR_BOX_DSC_H__

#include <vector>
#include <algorithm>
#include "ZmvsVehicleAbstractGearBoxDsc.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleDiscreteGearBoxDsc
	: public VehicleAbstractGearBoxDsc
{
public:
    typedef std::vector< Real > GearsRatio;
    typedef std::vector< Real >::iterator GearsRatioIterator;
    typedef std::vector< Real >::const_iterator GearsRatioConstIterator;

public:
    static const Real RATIONAL_GEAR_SWITCH_MIN_DELAI;
	static const Real RATIONAL_GEAR_SWITCH_MAX_DELAI;
	static const Real DEFAULT_GEAR_SWITCH_DELAI;

public:
    void defaultConfig();
	bool isValid() const;
	VehicleDiscreteGearBoxDsc* clone() const;
	bool print( std::ostream& _stream ) const;
	const char* getClassName() const;
	void accept( VehicleGearBoxDscVisitorInterface& _visitor );

public:
	GearsRatio gearsRatio;
};

}

#endif // __ZMVS_VEHICLE_DISCRETE_GEAR_BOX_DSC_H__