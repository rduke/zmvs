#include "stdafx.h"

using namespace Zmvs;

VehicleAbstractGearBoxMotion::
VehicleAbstractGearBoxMotion( VehicleAbstractGearBoxMotionDsc* _dsc )
: AbstractObject( _dsc ),
  m_Motor( SDKFactory::getSingleton()->createMotor( _dsc->motor.get() ) ),
  m_Transmission( SDKFactory::getSingleton()->createTransmission( _dsc->transmission.get() ) ),
  m_GearBox( SDKFactory::getSingleton()->createGearBox( _dsc->gearBox.get() ) )
{

}

VehicleAbstractMotor* VehicleAbstractGearBoxMotion::getMotor() const
{
	return m_Motor.get();
}

VehicleTransmission* VehicleAbstractGearBoxMotion::getTransmission() const
{
    return m_Transmission.get();
}

const char* VehicleAbstractGearBoxMotion::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleAbstractGearBoxMotion ];
}

VehicleAbstractGearBox* VehicleAbstractGearBoxMotion::getGearBox() const
{
	return m_GearBox.get();
}
