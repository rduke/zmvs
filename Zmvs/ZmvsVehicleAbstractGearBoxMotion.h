#ifndef __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_MOTION_H__
#define __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_MOTION_H__

#include "ZmvsVehicleCombustionMotor.h"
#include "ZmvsVehicleAbstractGearBoxMotionDsc.h"
#include "ZmvsVehicleAbstractGearBox.h"
#include "ZmvsVehicleAbstractGearBoxMotionInterface.h"
#include "ZmvsVehicleTransmission.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleAbstractGearBoxMotion
	: public AbstractObject,
	  public VehicleAbstractGearBoxMotionInterface
{
public:
	VehicleAbstractGearBoxMotion( VehicleAbstractGearBoxMotionDsc* _dsc );
	virtual VehicleAbstractMotor* getMotor() const;
    virtual VehicleTransmission*    getTransmission() const;
    const char* getClassName() const;
	virtual VehicleAbstractGearBox* getGearBox() const;
private:
	std::auto_ptr< VehicleTransmission >    m_Transmission;
	std::auto_ptr< VehicleAbstractMotor >   m_Motor;
	std::auto_ptr< VehicleAbstractGearBox > m_GearBox;
};

}

#endif // __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_MOTION_H__