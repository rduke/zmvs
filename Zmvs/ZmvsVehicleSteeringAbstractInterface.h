#ifndef __ZMVS_VEHICLE_STEERING_ABSTRACT_INTERFACE_H__
#define __ZMVS_VEHICLE_STEERING_ABSTRACT_INTERFACE_H__

//#include "ZmvsVehicleSteeringDsc.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleSteeringAbstractInterface
{
public:
	virtual void left( Real _deltaTime ) = 0;
	virtual void right( Real _deltaTime ) = 0;
	virtual void update( Real _deltaTime ) = 0;
	virtual bool setAngle( Real _angle ) = 0;
	virtual void setMaxLeft() = 0;
	virtual void setMaxRight() = 0;
	virtual void setStraight() = 0;
	virtual bool setSensibility( Real _value ) = 0;
	virtual bool setBackSensibility( Real _value ) = 0;
	virtual Real getAngle() const = 0;
	virtual ~VehicleSteeringAbstractInterface() {}
};

}

#endif // __ZMVS_VEHICLE_STEERING_ABSTRACT_INTERFACE_H__