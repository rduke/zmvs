#ifndef __ABSTRACT_OBJECT_DESCRIPTION_H__
#define __ABSTRACT_OBJECT_DESCRIPTION_H__

#include "ZmvsClassName.h"

namespace Zmvs
{

class AbstractObjectDescription
	: public ClassName
{
public:
	virtual AbstractObjectDescription* clone() const = 0;
	virtual bool isValid() const = 0;
	virtual void defaultConfig() = 0;
	virtual ~AbstractObjectDescription() { }
};

}

#endif // __ABSTRACT_OBJECT_DESCRIPTION_H__