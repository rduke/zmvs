#ifndef __ZMVS_VEHICLE_GEARBOX_DSC_VISITOR_INTERFACE_H__
#define __ZMVS_VEHICLE_GEARBOX_DSC_VISITOR_INTERFACE_H__

namespace Zmvs
{

class VehicleContinuousGearBoxDsc;
class VehicleDiscreteGearBoxDsc;

class VehicleGearBoxDscVisitorInterface
{
public:
	virtual void visit( VehicleContinuousGearBoxDsc& _dsc ) = 0;
    virtual void visit( VehicleDiscreteGearBoxDsc& _dsc ) = 0;
	virtual ~VehicleGearBoxDscVisitorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_GEARBOX_DSC_VISITOR_INTERFACE_H__