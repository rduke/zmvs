#ifndef __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_MOTION_H__
#define __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_MOTION_H__

#include "ZmvsVehicleAbstractGearBoxMotion.h"
#include "ZmvsVehicleContinuousGearBoxMotionDsc.h"

namespace Zmvs
{

class VehicleContinuousGearBoxMotion
	: public VehicleAbstractGearBoxMotion
{
public:
	VehicleContinuousGearBoxMotion( VehicleContinuousGearBoxMotionDsc* _dsc );
	VehicleContinuousGearBox* getGearBox() const;
	void update( Real _deltaTime );
	void accelerate( Real _deltaTime );
	void deccelerate( Real _deltaTime );
	VehicleCombustionMotor* getMotor() const;
	Real getTorque() const;
	Real getRotation() const;
	const char* getClassName() const;
};

}

#endif //  __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_MOTION_H__