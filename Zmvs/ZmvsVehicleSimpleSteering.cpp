#include "stdafx.h"

using namespace Zmvs;

VehicleSimpleSteering::VehicleSimpleSteering( VehicleSimpleSteeringDsc* _dsc )
: VehicleAbstractSteering( _dsc )
{

}

VehicleSimpleSteeringDsc* VehicleSimpleSteering::getDescription() const
{
	return static_cast< VehicleSimpleSteeringDsc* >( VehicleAbstractSteering::getDescription() );
}

bool VehicleSimpleSteering::setSpeed( Real _value )
{
	if( _value < 0.0f || _value < getDescription()->backSpeed )
		return false;
	
	getDescription()->speed = _value;
	return true;
}

bool VehicleSimpleSteering::setBackSpeed( Real _value )
{
	if( _value < 0.0f || _value > getDescription()->speed )
		return false;
	
	getDescription()->backSpeed = _value;
	return true;
}

void VehicleSimpleSteering::left( Real _deltaTime )
{
	if( getAngle() < getDescription()->maxAngle - getDescription()->speed * _deltaTime )
	    setAngle( getAngle() + getDescription()->speed * _deltaTime );
	else
		maxLeft();
}

void VehicleSimpleSteering::right( Real _deltaTime )
{
	if( getAngle() > -getDescription()->maxAngle + getDescription()->speed * _deltaTime )
	    setAngle( getAngle() - getDescription()->speed * _deltaTime );
	else
		maxRight();
}

bool VehicleSimpleSteering::setAngle( Real _angle )
{
	if( std::abs( _angle ) > getDescription()->maxAngle )
		return false;

	VehicleAbstractSteering::setAngle( _angle );
	return true;
}

void VehicleSimpleSteering::update( Real _deltaTime )
{
	if( std::abs( getAngle() ) < _deltaTime * getDescription()->backSpeed )
		setAngle( 0.0f );
	else if( getAngle() > _deltaTime * getDescription()->backSpeed )
		setAngle( getAngle() - _deltaTime * getDescription()->backSpeed );
	else
	    setAngle( getAngle() + _deltaTime * getDescription()->backSpeed );
}

void VehicleSimpleSteering::inverse()
{
	getDescription()->maxAngle = -getDescription()->maxAngle;
	getDescription()->speed    = -getDescription()->speed;
}

void VehicleSimpleSteering::maxLeft()
{
	setAngle( getDescription()->maxAngle );
}

void VehicleSimpleSteering::maxRight()
{
    setAngle( -getDescription()->maxAngle );
}

void VehicleSimpleSteering::straight()
{
    setAngle( 0.0f );
}

