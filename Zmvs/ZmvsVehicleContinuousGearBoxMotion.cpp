#include "stdafx.h"

using namespace Zmvs;

VehicleContinuousGearBoxMotion::VehicleContinuousGearBoxMotion( VehicleContinuousGearBoxMotionDsc* _dsc )
: VehicleAbstractGearBoxMotion( _dsc )
{

}

Real VehicleContinuousGearBoxMotion::getRotation() const
{
	return getMotor()->getRotation() / getGearBox()->getRatio();
}

Real VehicleContinuousGearBoxMotion::getTorque() const
{
	return getMotor()->getTorque() * getGearBox()->getRatio(); // PROBLEM IS HERE
}

void VehicleContinuousGearBoxMotion::accelerate( Real _deltaTime )
{
	getMotor()->accelerate( _deltaTime );
}

void VehicleContinuousGearBoxMotion::deccelerate( Real _deltaTime )
{
	getMotor()->deccelerate( _deltaTime );
}

VehicleCombustionMotor* VehicleContinuousGearBoxMotion::getMotor() const
{
	return static_cast< VehicleCombustionMotor* >( VehicleAbstractGearBoxMotion::getMotor() );
}

void VehicleContinuousGearBoxMotion::update( Real _deltaTime )
{
	getMotor()->update( _deltaTime );
	getGearBox()->setMotorRpm( getMotor()->getRpm() );
	getGearBox()->update( _deltaTime );
	getTransmission()->update( getTorque(), getRotation() );
}

const char* VehicleContinuousGearBoxMotion::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleContinuousGearBoxMotion ];
}

VehicleContinuousGearBox* VehicleContinuousGearBoxMotion::getGearBox() const
{
	return static_cast< VehicleContinuousGearBox* >( VehicleAbstractGearBoxMotion::getGearBox() );
}