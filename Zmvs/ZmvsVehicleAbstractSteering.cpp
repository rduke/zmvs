#include "stdafx.h"

using namespace Zmvs;

VehicleAbstractSteering::VehicleAbstractSteering( VehicleAbstractSteeringDsc* _dsc )
: AbstractObject( _dsc ),
  m_Angle( _dsc->initialAngle )
{

}

bool VehicleAbstractSteering::setAngle( Real _angle )
{
	m_Angle = _angle;
	return true;
}

Real VehicleAbstractSteering::getAngle() const
{
    return m_Angle;
}