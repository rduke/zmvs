#ifndef __ZMVS_VEHICLE_DISCRETE_GEAR_BOX_H__
#define __ZMVS_VEHICLE_DISCRETE_GEAR_BOX_H__

#include "ZmvsVehicleDiscreteGearBoxDsc.h"
#include "ZmvsVehicleAbstractGearBox.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleDiscreteGearBox
	: public VehicleAbstractGearBox
{
public:
	VehicleDiscreteGearBox( VehicleDiscreteGearBoxDsc* _dsc );
    Real getRatio( size_t _gear ) const;
	Real getRatio() const;
	VehicleDiscreteGearBoxDsc* getDescription() const;
	void up( Real _deltaTime );
	void down( Real _deltaTime );
	bool setGear( size_t _currGear );
    size_t getMaxGear() const;
    size_t getGear() const;
    void update( Real _deltaTime );
	bool isNeutral() const;
	bool setSwitchDelai( Real _deltaTime );
public:
	enum gearBoxStates
	{
		REAR,
		NEUTRAL,
		FIRST,
		SECOND,
		THIRD,
		FOURTH,
		FIFTH
	};
    static const Real GEAR_OUT_OF_RANGE;
protected:
	size_t m_Gear;
};

}

#endif // __ZMVS_VEHICLE_DISCRETE_GEAR_BOX_H__