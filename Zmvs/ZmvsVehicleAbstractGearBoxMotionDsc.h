#ifndef __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_MOTION_DSC_H__
#define __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_MOTION_DSC_H__

#include "ZmvsVehicleCombustionMotorDsc.h"
#include "ZmvsVehicleTransmissionDsc.h"
#include "ZmvsAbstractObjectDescription.h"
#include "ZmvsVehicleAbstractGearBoxDsc.h"
#include "ZmvsVehicleGearBoxMotionDscVisitsAcceptorInterface.h"
#include <memory>

namespace Zmvs
{

class ZmvsPublicClass VehicleAbstractGearBoxMotionDsc
	: public AbstractObjectDescription,
	  public VehicleGearBoxMotionDscVisitsAcceptorInterface
{
public:
	VehicleAbstractGearBoxMotionDsc( VehicleAbstractGearBoxDsc* _dsc );
	virtual void defaultConfig();
    const char* getClassName() const;
	bool isValid() const;

public:
	std::auto_ptr< VehicleCombustionMotorDsc > motor;
	std::auto_ptr< VehicleTransmissionDsc >    transmission;
	std::auto_ptr< VehicleAbstractGearBoxDsc > gearBox;
};

}

#endif // __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_MOTION_DSC_H__