#ifndef __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_H__
#define __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_H__

#include "ZmvsVehicleContinuousGearBoxDsc.h"

namespace Zmvs
{

class VehicleContinuousGearBox
	: public VehicleAbstractGearBox
{
public:
		enum VehicleContinuousGearBoxStates
	{
		VEHICLE_CONTINUOUS_GEAR_BOX_REAR,
		VEHICLE_CONTINUOUS_GEAR_BOX_NEUTRAL,
		VEHICLE_CONTINUOUS_GEAR_BOX_FORWARD
	};
public:
	VehicleContinuousGearBox( VehicleContinuousGearBoxDsc* _dsc );
	void update( Real _deltaTime );
	Real getRatio() const;
	VehicleContinuousGearBoxDsc* getDescription() const;
	VehicleContinuousGearBoxStates getState() const;
	void up( Real _deltaTime );
	void down( Real _deltaTime );
	void setMotorRpm( Real _rpm );
	bool isNeutral() const;
	bool setSwitchDelai( Real _delai );

private:
    VehicleContinuousGearBoxStates m_State;
	Real m_Ratio;
	Real m_MotorRpm;
};

}

#endif // __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_H__