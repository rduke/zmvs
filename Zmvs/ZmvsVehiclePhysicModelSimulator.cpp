#include "stdafx.h"

using namespace Zmvs;

const Real VehiclePhysicModelSimulator::G_CONSTANT_0_013606 = 0.013606f;
const Real VehiclePhysicModelSimulator::G_CONSTANT_0_00033  = 0.00033f;
const Real VehiclePhysicModelSimulator::G_CONSTANT_8_10e_5 = 8.10e-5f;