#ifndef __ZMVS_CLASS_NAME_H__
#define __ZMVS_CLASS_NAME_H__

namespace Zmvs
{

class ClassName
{
public:
	static const char* ClassNameBuffer[ 500 ];
	enum Class_Name
	{
		_Object                    = 0,
		_ObjectDesc,
		_VehicleGearBoxMotionListenable,
		_VehicleAbstractGearBoxDsc,
        _VehicleDiscreteGearBoxDsc,
		_VehicleContinuousGearBoxDsc,
		_VehicleAbstractGearBox,
		_VehicleDiscreteGearBox,
		_VehicleContinuousGearBox,
		_VehicleAbstractGearBoxMotionDsc,
		_VehicleManualGearBoxMotionDsc,
		_VehicleContinuousGearBoxMotionDsc,
		_VehicleAutoGearBoxMotionDsc,
		_VehicleSpecialAutoGearBoxMotionDsc,
        _VehicleAbstractGearBoxMotion,
		_VehicleManualGearBoxMotion,
		_VehicleContinuousGearBoxMotion,
		_VehicleAutoGearBoxMotion,
		_VehicleSpecialAutoGearBoxMotion
	};
};

}

#endif // __ZMVS_CLASS_NAME_H__