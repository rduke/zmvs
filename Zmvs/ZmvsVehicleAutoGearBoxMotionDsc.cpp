#include "stdafx.h"

using namespace Zmvs;

VehicleAutoGearBoxMotionDsc* VehicleAutoGearBoxMotionDsc::clone() const
{
	return static_cast< VehicleAutoGearBoxMotionDsc* >( VehicleManualGearBoxMotionDsc::clone() );
}

const char* VehicleAutoGearBoxMotionDsc::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleAutoGearBoxMotionDsc ];
}

void VehicleAutoGearBoxMotionDsc::accept( VehicleGearBoxMotionDscVisitorInterface& _visitor )
{
    _visitor.visit( *this );
}