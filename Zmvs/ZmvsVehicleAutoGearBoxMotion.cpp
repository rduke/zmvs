#include "stdafx.h"

using namespace Zmvs;

VehicleAutoGearBoxMotion::VehicleAutoGearBoxMotion( VehicleAutoGearBoxMotionDsc* _dsc )
: VehicleManualGearBoxMotion( _dsc ),
  m_AutoGearBoxState( Auto_GearBox_Neutral )
{

}

VehicleAutoGearBoxMotion::Auto_GearBox_States VehicleAutoGearBoxMotion::getGear() const
{
    return m_AutoGearBoxState;
}

void VehicleAutoGearBoxMotion::setGear( Auto_GearBox_States _s )
{
	m_AutoGearBoxState = _s;
}


void VehicleAutoGearBoxMotion::accelerate( Real _deltaTime )
{
	if( m_AutoGearBoxState == Auto_GearBox_Drive )
	{
		if( getMotor()->getRpm() < getMotor()->getDescription()->maxRpmToGearUp )
			getMotor()->accelerate( _deltaTime );
		else
			getGearBox()->up( _deltaTime );
	}

	else if( m_AutoGearBoxState == Auto_GearBox_Rear )
	{
		if( getMotor()->getRpm() < getMotor()->getDescription()->maxRpm )
			getMotor()->accelerate( _deltaTime );
	}
		
	else if( m_AutoGearBoxState == Auto_GearBox_Neutral )
	{
		getMotor()->accelerate( _deltaTime ); // TO DO
	}

	else
		getMotor()->accelerate( _deltaTime );
}

void VehicleAutoGearBoxMotion::deccelerate( Real _deltaTime )
{
    if( m_AutoGearBoxState == Auto_GearBox_Drive )
		if( getMotor()->getRpm() > getMotor()->getDescription()->minRpmToGearDown )
			getMotor()->deccelerate( _deltaTime );
		else
			if( getGearBox()->getGear() > VehicleDiscreteGearBox::FIRST )
			    getGearBox()->down( _deltaTime );
			else 
				getMotor()->deccelerate( _deltaTime );
			

	else if( m_AutoGearBoxState == Auto_GearBox_Rear )
			getMotor()->deccelerate( _deltaTime );
		
	else
		getMotor()->deccelerate( _deltaTime );
}

bool VehicleAutoGearBoxMotion::gearUp()
{
	m_AutoGearBoxState = Auto_GearBox_Drive;
	VehicleManualGearBoxMotion::gearUp();
	return true;
}

bool VehicleAutoGearBoxMotion::gearDown()
{
	m_AutoGearBoxState = Auto_GearBox_Drive;
	getGearBox()->setGear( VehicleDiscreteGearBox::REAR );
	return true;
}

bool VehicleAutoGearBoxMotion::gearNeutral()
{
	m_AutoGearBoxState = Auto_GearBox_Neutral;
	VehicleManualGearBoxMotion::gearNeutral();
	return true;
}

const char* VehicleAutoGearBoxMotion::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleAutoGearBoxMotion ];
}