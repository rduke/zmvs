#include "stdafx.h"

using namespace Zmvs;

const Real VehicleContinuousGearBoxDsc::RATIO_INTERVAL_RATIONAL_LOWER_LIMIT = -10.0f;
const Real VehicleContinuousGearBoxDsc::RATIO_INTERVAL_RATIONAL_UPPER_LIMIT = 10.0f;
//const NumericInterval VehicleContinuousGearBoxDsc::DEFAULT_RATIO_INTERVAL = NumericInterval( -2.8f, 0.5f );
const Real VehicleContinuousGearBoxDsc::DEFAULT_MOTOR_RPM = 3000.0f;
const Real VehicleContinuousGearBoxDsc::RATIONAL_GEAR_SWITCH_MIN_DELAI = 0.050f;
const Real VehicleContinuousGearBoxDsc::RATIONAL_GEAR_SWITCH_MAX_DELAI = 5.0f;
const Real VehicleContinuousGearBoxDsc::DEFAULT_SWITCH_DELAI = 0.1f;
const Real VehicleContinuousGearBoxDsc::DEFAULT_DISCRETISATION_COEFF = 0.05f;
const Real VehicleContinuousGearBoxDsc::DEFAULT_MIN_DISCRETISATION_COEFF = 0.01f;
const Real VehicleContinuousGearBoxDsc::DEFAULT_MAX_DISCRETISATION_COEFF = 0.99f;

void VehicleContinuousGearBoxDsc::defaultConfig()
{
    minRatio = 0.5f;
	maxRatio = 2.5f;
	motorRpm = DEFAULT_MOTOR_RPM;
    switchDelai = DEFAULT_SWITCH_DELAI;
	discretisationCoeff = DEFAULT_DISCRETISATION_COEFF;
}

bool VehicleContinuousGearBoxDsc::isValid() const
{
	return ( motorRpm > 0.0f &&
			 switchDelai > RATIONAL_GEAR_SWITCH_MIN_DELAI &&
			 switchDelai < RATIONAL_GEAR_SWITCH_MAX_DELAI &&
			 Math::clamp( discretisationCoeff,
			              DEFAULT_MIN_DISCRETISATION_COEFF,
			              DEFAULT_MAX_DISCRETISATION_COEFF ) );
}

VehicleContinuousGearBoxDsc* VehicleContinuousGearBoxDsc::clone() const
{
	return new VehicleContinuousGearBoxDsc( *this );
}

void VehicleContinuousGearBoxDsc::accept( VehicleGearBoxDscVisitorInterface& _visitor )
{
	_visitor.visit( *this );
}