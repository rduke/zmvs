#include "stdafx.h"

using namespace Zmvs;

VehicleAdaptativeSteering::VehicleAdaptativeSteering( VehicleAdaptativeSteeringDsc* _dsc )
: VehicleAbstractSteering( _dsc )
{

}

VehicleAdaptativeSteeringDsc* VehicleAdaptativeSteering::getDescription() const
{
	return static_cast< VehicleAdaptativeSteeringDsc* >( VehicleAbstractSteering::getDescription() );
}

void VehicleAdaptativeSteering::maxLeft()
{
	setAngle(getDescription()->maxAngle);
}

void VehicleAdaptativeSteering::maxRight()
{
    setAngle(-getDescription()->maxAngle);
}

void VehicleAdaptativeSteering::straight()
{
    setAngle(0.0f);
}

void VehicleAdaptativeSteering::notify( Real _chassisSpeed )
{
    m_ChassisSpeed = _chassisSpeed;
}

bool VehicleAdaptativeSteering::setSpeed( Real _value )
{
	if( _value < 0.0f || _value < getDescription()->backSpeed )
		return false;
	
	getDescription()->speed = _value;
	return true;
}

bool VehicleAdaptativeSteering::setBackSpeed( Real _value )
{
	if( _value < 0.0f || _value > getDescription()->speed )
		return false;
	
	getDescription()->backSpeed = _value;
	return true;
}

void VehicleAdaptativeSteering::left( Real _deltaTime )
{
	if( getAngle() < getDescription()->maxAngle - getDescription()->speed * _deltaTime * m_ChassisSpeed / getDescription()->adaptativeCoeff )
	    setAngle( getAngle() + getDescription()->speed * _deltaTime * m_ChassisSpeed / getDescription()->adaptativeCoeff );
	else
		maxLeft();
}

void VehicleAdaptativeSteering::right( Real _deltaTime )
{
	if( getAngle() > -getDescription()->maxAngle + getDescription()->speed * _deltaTime * m_ChassisSpeed / getDescription()->adaptativeCoeff )
	    setAngle( getAngle() - getDescription()->speed * _deltaTime * m_ChassisSpeed / getDescription()->adaptativeCoeff );
	else
		maxRight();
}

bool VehicleAdaptativeSteering::setAngle( Real _angle )
{
	if( std::abs( _angle ) > getDescription()->maxAngle )
		return false;

	VehicleAbstractSteering::setAngle( _angle );
	return true;
}

void VehicleAdaptativeSteering::update( Real _deltaTime )
{
	if( std::abs( getAngle() ) > _deltaTime * getDescription()->backSpeed * m_ChassisSpeed / getDescription()->adaptativeCoeff )
		setAngle( 0.0f );
	else if( getAngle() > _deltaTime * getDescription()->backSpeed * m_ChassisSpeed / getDescription()->adaptativeCoeff )
		setAngle( getAngle() - _deltaTime * getDescription()->backSpeed * m_ChassisSpeed / getDescription()->adaptativeCoeff );
	else
	    setAngle( getAngle() + _deltaTime * getDescription()->backSpeed * m_ChassisSpeed / getDescription()->adaptativeCoeff );
}

void VehicleAdaptativeSteering::inverse()
{
    getDescription()->maxAngle = -getDescription()->maxAngle;
	getDescription()->speed = -getDescription()->speed;
}

