#ifndef __ZMVS_VEHICLE_SIMPLE_STEERING_DSC_H__
#define __ZMVS_VEHICLE_SIMPLE_STEERING_DSC_H__

#include "ZmvsVehicleAbstractSteeringDsc.h"

namespace Zmvs
{

class VehicleSimpleSteeringDsc
	: public VehicleAbstractSteeringDsc
{
public:
	VehicleSimpleSteeringDsc* clone() const;
	void accept( VehicleSteeringDscVisitorInterface& _visitor );
};

}

#endif // __ZMVS_VEHICLE_SIMPLE_STEERING_DSC_H__