#ifndef __ZMVS_VEHICLE_MANUAL_GEAR_BOX_MOTION_DSC_H__
#define __ZMVS_VEHICLE_MANUAL_GEAR_BOX_MOTION_DSC_H__

#include "ZmvsVehicleAbstractGearBoxMotionDsc.h"
#include "ZmvsVehicleDiscreteGearBoxDsc.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleManualGearBoxMotionDsc
	: public VehicleAbstractGearBoxMotionDsc
{
public:
	VehicleManualGearBoxMotionDsc();
	const char* getClassName() const;
	VehicleManualGearBoxMotionDsc* clone() const;
	void accept( VehicleGearBoxMotionDscVisitorInterface& _visitor );
};

}


#endif // __ZMVS_VEHICLE_MANUAL_GEAR_BOX_MOTION_DSC_H__