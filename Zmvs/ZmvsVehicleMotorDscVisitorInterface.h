#ifndef __ZMVS_VEHICLE_MOTOR_DSC_VISITOR_INTERFACE_H__
#define __ZMVS_VEHICLE_MOTOR_DSC_VISITOR_INTERFACE_H__

namespace Zmvs
{

class VehicleCombustionMotorDsc;

class VehicleMotorDscVisitorInterface
{
public:
	virtual void visit( VehicleCombustionMotorDsc& _dsc ) = 0;
	virtual ~VehicleMotorDscVisitorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_MOTOR_DSC_VISITOR_INTERFACE_H__