#include "stdafx.h"

using namespace Zmvs;

const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_RPM = 6000.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_RPM = 1000.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_ACCE_SPEED = 1.5f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_DECCEL_SPEED = 0.5f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_RPM_TO_GEAR_DOWN = 1500.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_RPM_TO_GEAR_UP = 4500.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_TORQUE = 500.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_TORQUE = 10.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_SPEED  = 200.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_REAR_SPEED = 70.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_RPM_GAIN_COEEF = 0.3f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_RPM_LOOSE_COEEF = 0.3f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_DEFAULT_STARTING_DELAI = 500.0f;
const Real VehicleCombustionMotorDsc::VEHICLE_COMBUSTION_MOTOR_MIN_STARTING_DELAI = 100.0f;

VehicleCombustionMotorDsc* VehicleCombustionMotorDsc::clone() const
{
	return new VehicleCombustionMotorDsc( *this );
}

bool VehicleCombustionMotorDsc::print( std::ostream& _out ) const
{
	_out << "minRpmToGearDown = " << minRpmToGearDown << std::endl;
	_out << "maxRpmToGearUp = " << maxRpmToGearUp << std::endl;
	_out << "minRpm = " << minRpm << std::endl;
	_out << "maxRpm = " << maxRpm << std::endl;
	_out << "accelDelai = " << accelDelai << std::endl;
	_out << "deccelDelai = " << deccelDelai << std::endl;
	_out << "torqueCurve values list " << std::endl;
	_out << "begin" << std::endl;
	torqueCurve.print( _out );
	_out << "end" << std::endl;
	return true;
}

void VehicleCombustionMotorDsc::accept( VehicleMotorDscVisitorInterface& _visitor )
{
    _visitor.visit( *this );
}

void VehicleCombustionMotorDsc::defaultConfig()
{
    minRpmToGearDown = VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_RPM_TO_GEAR_DOWN;
	maxRpmToGearUp   = VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_RPM_TO_GEAR_UP;
	minRpm           = VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_RPM;
	maxRpm           = VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_RPM;

	torqueCurve.insert(1000.f, 393.f);
	torqueCurve.insert(2000.f, 434.f);
	torqueCurve.insert(4000.f, 475.f);
	torqueCurve.insert(5000.f, 475.f);
	torqueCurve.insert(6000.f, 366.f);

	accelDelai       = VEHICLE_COMBUSTION_MOTOR_DEFAULT_ACCE_SPEED;
	deccelDelai	     = VEHICLE_COMBUSTION_MOTOR_DEFAULT_DECCEL_SPEED;
}

bool VehicleCombustionMotorDsc::isValid() const
{
	return !( minRpmToGearDown  <= 0.0f            ||
		      maxRpmToGearUp    <= 0.0f            ||
		      minRpm	        <= 0.0f            ||
		      maxRpm            <= 0.0f            ||
		      accelDelai        <= 0.0f            ||
		      deccelDelai       <= 0.0f            ||
		      accelDelai        < deccelDelai      ||
		      maxRpmToGearUp    < minRpmToGearDown || 
		      maxRpm            < minRpm           ||  
			  torqueCurve.isEmpty()                );
}