#ifndef __ZMVS_AUTO_CONFIG_H__
#define __ZMVS_AUTO_CONFIG_H__


#include "ZmvsConfig.h"


#define ZmvsRealAccurancyFloat 1
#define ZmvsRealAccurancyDouble 2
#define ZmvsCompilerUnknown 0
#define ZmvsCompilerMSVC    1
#define ZmvsCompilerGNUC    2

#define ZmvsPlatformUnknown 0
#define ZmvsPlatformWindows 1
#define ZmvsPlatformLinux   2

#define ZmvsArchitecture32Bit 32
#define ZmvsArchitecture64Bit 64

//#define ZMVS_LIBRARY_OPTIONS
#define ZMVS_DYNAMIC_LIBRARY 0
#define ZMVS_STATIC_LIBRARY  1

//#define ZMVS_DYMANIC_LIBRARY_OPTIONS
#define ZMVS_DYNAMIC_LIBRARY_OPTIONS_EXPORT 0
#define ZMVS_DYNAMIC_LIBRARY_OPTIONS_IMPORT 1

//#define ZMVS_VEHICLE_FACTORY_INVALID_DESCRIPTION_PROTOCOL
#define ZMVS_VEHICLE_FACTORY_INVALID_DESCRIPTION_RETURN_NULL 0
#define ZMVS_VEHICLE_FACTORY_INVALID_DESCRIPTION_THROW 1

//#define ZMVS_DSC_DEFAULT_CONFIG 0
#define ZMVS_DSC_AUTO_DEFAULT_CONFIG 0
#define ZMVS_DSC_MANUAL_DEFAULT_CONFIG 1

#define ZMVS_OBJECT_DESCRIPTION_WITHOUT_CHECK  0
#define ZMVS_OBJECT_DESCRIPTION_QUICK_CHECK    1
#define ZMVS_OBJECT_DESCRIPTION_COMPLETE_CHECK 2

#define ZMVS_STRING2( _ZMVS_STRING_ ) #_ZMVS_STRING_
#define ZMVS_TO_STRING( _ZMVS_STRING_ ) ZMVS_STRING2( _ZMVS_STRING_ )

#if ZmoCompiler == ZmoCompilerMSVC
 #define ZMVS_WARNING( _ZMVS_WARNING_ ) __pragma( warning( _ZMVS_WARNING_ ) )
 #define ZMVS_MESSAGE( _ZMVS_MESSAGE_ ) __pragma( message( _ZMVS_MESSAGE_ ) )
 #define ZMVS_WARNING_MESSAGE( _ZMVS_WARNING_MESSAGE_ ) \
     ZMVS_MESSAGE( "warning from Zmo library " __FILE__ "[" ZMVS_TO_STRING( __LINE__ ) "] : " _ZMVS_WARNING_MESSAGE_ )
 #define ZMVS_ERROR( _ZMVS_ERROR_TEXT_ ) __pragma( error( _ZMVS_ERROR_ ) )
#elif ZmoCompiler == ZmoCompilerGNUC
 #define ZMVS_WARNING( _ZMVS_WARNING_TEXT_ ) __warning( _ZMVS_WARNING_TEXT_ )
 #define ZMVS_MESSAGE( _ZMVS_NOTIFICATION_TEXT_ ) __message( _ZMVS_NOTIFICATION_TEXT_ )
 #define ZMVS_ERROR( _ZMVS_ERROR_TEXT_ ) __error( _ZMVS_ERROR_TEXT_ )
#else
 #define ZMVS_WARNING( _ZMVS_WARNING_TEXT_ ) { }
 #define ZMVS_MESSAGE( _ZMVS_NOTIFICATION_TEXT_ ) { }
 #define ZMVS_ERROR( _ZMVS_ERROR_TEXT_ ) { }
#endif


#if defined ( _MSC_VER )
# define ZmvsCompiler ZmvsCompilerMSVC
#elif defined ( __GNUC__ )
# define ZmvsCompiler ZmvsCompilerGNUC
#endif

#if defined (_WIN32) || defined (__WIN32)
# define ZmvsPlatform ZmvsPlatformWindows
# define NOMINMAX
#elif defined (LINUX) || defined(_LINUX)
# define ZmvsPlatform ZmvsPlatformLinux
#endif

#if defined( _M_X64 ) || defined ( _M_X64 )
# define ZmvsArchitecture ZmvsArchitecture64Bit
#else
# define ZmvsArchitecture ZmvsArchitecture32Bit
#endif


#if !defined( ZMVS_LIBRARY_OPTIONS )
 ZMVS_MESSAGE( "No library option was chosen Zmvs will be compiled as static library" )
 #define ZMVS_LIBRARY_OPTIONS ZMVS_STATIC_LIBRARY
#endif

#if !defined( ZMVS_DSC_DEFAULT_CONFIG )
 ZMVS_MESSAGE( "defaultConfig inside dsc constructor" )
 #define ZMVS_DSC_DEFAULT_CONFIG ZMVS_AUTO_DEFAULT_CONFIG
#endif

#if !defined( ZMVS_VEHICLE_FACTORY_INVALID_DESCRIPTION_PROTOCOL )
 ZMVS_MESSAGE( "vehicle factory invalid description protocol default throw" )
 #define ZMVS_VEHICLE_FACTORY_INVALID_DESCRIPTION_PROTOCOL ZMVS_VEHICLE_FACTORY_INVALID_DESCRIPTION_THROW
#endif

#if ZMVS_LIBRARY_OPTIONS == ZMVS_DYNAMIC_LIBRARY 
 #if ZMVS_DYNAMIC_LIBRARY_OPTIONS == ZMVS_DYNAMIC_LIBRARY_OPTIONS_EXPORT
  #define ZmvsPublicClass         __declspec( dllexport )
  #define ZmvsPublicFunction      __declspec( dllexport )
  #define ZmvsPublicTemplateClass
 #elif ZMVS_DYNAMIC_LIBRARY_OPTIONS == ZMVS_DYNAMIC_LIBRARY_OPTIONS_IMPORT
  #define ZmvsPublicClass         __declspec( dllimport )
  #define ZmvsPublicFunction      __declspec( dllimport )
  #define ZmvsPublicTemplateClass
 #else
  #error Dynamic Library export/import options undefined!
 #endif
#elif ZMVS_LIBRARY_OPTIONS == ZMVS_STATIC_LIBRARY
 #define ZmvsPublicClass         
 #define ZmvsPublicFunction      
 #define ZmvsPublicTemplateClass
#else
 #error Library type undefined!
#endif

#if ( ZmvsCompiler == ZmvsCompilerMSVC )
 #define ZmvsForceInline __forceinline
#endif 

#if ( ZmvsCompiler == ZmvsCompilerGNUC )
# define ZmvsForceInline __inline
# define ZmvsPublicClass
# define ZmvsPublicFunction
#endif

#if defined( _DEBUG ) || defined( DEBUG )
# ifndef ZMVS_DEBUG
#  define ZMVS_DEBUG
# endif
#endif


#if ZmvsRealAccurancy == ZmvsRealAccurancyFloat
namespace Zmvs { typedef float Real; }
#elif ZmvsRealAccuray == ZmvsRealAccurancyDouble
namespace Zmvs { typedef double Real; }
#else 
#error Floating point accuracy not specified
#endif

#define ZMVS_THROW throw

#endif // __ZMVS_AUTO_CONFIG_H__