#ifndef __ZMVS_VEHICLE_STEERING_DSC_H__
#define __ZMVS_VEHICLE_STEERING_DSC_H__

#include "ZmvsCommon.h"
#include "ZmvsAbstractObjectDescription.h"
#include "ZmvsVehicleSteeringDscVisitsAcceptorInterface.h"
#include "ZmvsDebugOutput.h"

namespace Zmvs
{

class VehicleAbstractSteeringDsc
	: public AbstractObjectDescription,
	  public VehicleSteeringDscVisitsAcceptorInterface,
	  public DebugOutput< VehicleAbstractSteeringDsc >
{
public:
	virtual void defaultConfig();
	virtual bool isValid() const;
	bool print( std::ostream& _stream ) const;

public:
	static const Real VEHICLE_ABSTRACT_STEERING_MAX_ANGLE;

public:
	Real initialAngle;
	Real maxAngle;
	Real speed;
	Real backSpeed;
    	
};

}

#endif // __ZMVS_VEHICLE_STEERING_DSC_H__