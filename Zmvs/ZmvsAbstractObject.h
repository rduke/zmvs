#ifndef __ABSTRACT_OBJECT_H__
#define __ABSTRACT_OBJECT_H__

#include "ZmvsClassName.h"
#include <memory>
#include "ZmvsAbstractObjectDescription.h"

namespace Zmvs
{

class AbstractObject
	: public ClassName
{
public:
	AbstractObject( AbstractObjectDescription* _desc )
		: m_Description( _desc->clone() ) { }

	virtual AbstractObjectDescription* getDescription() const { return m_Description.get(); }
private:
	std::auto_ptr< AbstractObjectDescription > m_Description;
};

}

#endif // __ABSTRACT_OBJECT_H__