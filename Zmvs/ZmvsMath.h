#ifndef __ZMVS_MATH_H__
#define __ZMVS_MATH_H__

#include "ZmvsCommon.h"

namespace Zmvs
{
namespace Math
{

const Real EPSILON =  1.0e-7f;

inline Real isZero( Real _val )
{
    return ( fabs( _val ) < EPSILON );
}

enum AfterComa_Precision
{
	AfterComa_Precision_0 = 1,
    AfterComa_Precision_1 = 10,
	AfterComa_Precision_2 = 100,
	AfterComa_Precision_3 = 1000,
	AfterComa_Precision_4 = 10000,
	AfterComa_Precision_5 = 100000
};

template < typename T >
inline T normalize( T _value, AfterComa_Precision _precision )
{
    return ( ( T ) ( ( int )( _value * _precision ) ) ) / _precision;
}

template < typename T >
inline bool clamp( T _value, T _lower, T _upper )
{
    return ( _value < _upper &&
		     _value > _lower );
}

}

}

#endif // __ZMVS_MATH_H__