#ifndef __ZMVS_VEHICLE_AUTO_GEAR_BOX_MOTION_H__
#define __ZMVS_VEHICLE_AUTO_GEAR_BOX_MOTION_H__

#include "ZmvsVehicleAutoGearBoxMotionDsc.h"
#include "ZmvsVehicleManualGearBoxMotion.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleAutoGearBoxMotion
	: public VehicleManualGearBoxMotion
{
public:
	enum Auto_GearBox_States
	{
		Auto_GearBox_Parking,
		Auto_GearBox_Neutral,
		Auto_GearBox_Drive,
		Auto_GearBox_Rear
	};

public:
	VehicleAutoGearBoxMotion( VehicleAutoGearBoxMotionDsc* _dsc );
	void accelerate( Real _deltaTime );
	void deccelerate( Real _deltaTime );
	bool gearUp();
	bool gearDown();
	bool gearNeutral();
	virtual void setGear( Auto_GearBox_States _s );
	virtual Auto_GearBox_States getGear() const;
	const char* getClassName() const;

private:
	Auto_GearBox_States m_AutoGearBoxState;
};

}

#endif // __ZMVS_VEHICLE_AUTO_GEAR_BOX_MOTION_H__