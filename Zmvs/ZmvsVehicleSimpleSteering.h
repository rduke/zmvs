#ifndef __ZMVS_VEHICLE_SIMPLE_STEERING_H__
#define __ZMVS_VEHICLE_SIMPLE_STEERING_H__

#include "ZmvsVehicleAbstractSteering.h"
#include "ZmvsVehicleSimpleSteeringDsc.h"

namespace Zmvs
{

class VehicleSimpleSteeringInvalidDsc
{
};

class ZmvsPublicClass VehicleSimpleSteering
	: public VehicleAbstractSteering
{
public:
	VehicleSimpleSteering( VehicleSimpleSteeringDsc* _dsc );
	VehicleSimpleSteeringDsc* getDescription() const;
	void left( Real _deltaTime );
	void right( Real _deltaTime );
	void update( Real _deltaTime );
	bool setAngle( Real _angle );
	bool setSpeed( Real _value );
	bool setBackSpeed( Real _value );
	void maxLeft();
	void maxRight();
	void straight();
	void inverse();
};

}

#endif // __ZMVS_VEHICLE_SIMPLE_STEERING_H__