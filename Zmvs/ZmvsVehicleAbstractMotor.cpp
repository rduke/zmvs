#include "stdafx.h"

using namespace Zmvs;

const Real VehicleAbstractMotor::INITIAL_TORQUE = 0.0f;
const Real VehicleAbstractMotor::INITIAL_ROTATION = 0.0f;

VehicleAbstractMotor::VehicleAbstractMotor( VehicleAbstractMotorDsc* _dsc )
: AbstractObject( _dsc ),
  MotorizedObject( 0.0f, 0.0f ),
  m_Rpm( 0.0f )
{
}

VehicleAbstractMotorDsc* VehicleAbstractMotor::getDescription() const
{
	return static_cast< VehicleAbstractMotorDsc* >( AbstractObject::getDescription() );
}

void VehicleAbstractMotor::setRpm( Real _rpm )
{
    m_Rpm = _rpm;
}

Real VehicleAbstractMotor::getRpm() const
{
    return m_Rpm;
}
