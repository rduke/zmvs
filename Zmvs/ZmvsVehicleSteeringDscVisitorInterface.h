#ifndef __ZMVS_VEHICLE_STEERING_DSC_VISITOR_INTERFACE_H__
#define __ZMVS_VEHICLE_STEERING_DSC_VISITOR_INTERFACE_H__

namespace Zmvs
{

class VehicleAdaptativeSteeringDsc;
class VehicleSimpleSteeringDsc;

class VehicleSteeringDscVisitorInterface
{
public:
	virtual void visit( VehicleAdaptativeSteeringDsc& _dsc ) = 0;
    virtual void visit( VehicleSimpleSteeringDsc& _dsc ) = 0;
	virtual ~VehicleSteeringDscVisitorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_STEERING_DSC_VISITOR_INTERFACE_H__