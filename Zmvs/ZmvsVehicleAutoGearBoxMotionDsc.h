#ifndef __ZMVS_VEHICLE_AUTO_GEAR_BOX_MOTION_DSC_H__
#define __ZMVS_VEHICLE_AUTO_GEAR_BOX_MOTION_DSC_H__

#include "ZmvsVehicleManualGearBoxMotionDsc.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleAutoGearBoxMotionDsc
	: public VehicleManualGearBoxMotionDsc
{
public:
	VehicleAutoGearBoxMotionDsc* clone() const;
	const char* getClassName() const;
	void accept( VehicleGearBoxMotionDscVisitorInterface& _visitor );
};

}

#endif // __ZMVS_VEHICLE_AUTO_GEAR_BOX_MOTION_DSC_H__
