#ifndef __ZMVS_VEHICLE_GEARBOX_MOTION_DSC_VISITS_ACCEPTOR_INTERFACE_H__
#define __ZMVS_VEHICLE_GEARBOX_MOTION_DSC_VISITS_ACCEPTOR_INTERFACE_H__

#include "ZmvsVehicleGearBoxMotionDscVisitorInterface.h"

namespace Zmvs
{

class VehicleGearBoxMotionDscVisitsAcceptorInterface
{
public:
	virtual void accept( VehicleGearBoxMotionDscVisitorInterface& _dsc ) = 0;
	virtual ~VehicleGearBoxMotionDscVisitsAcceptorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_GEARBOX_MOTION_DSC_VISITS_ACCEPTOR_INTERFACE_H__