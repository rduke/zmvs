#ifndef __ZMVS_VEHICLE_ABSTRACT_GEARBOX_MOTION_INTERFACE_H__
#define __ZMVS_VEHICLE_ABSTRACT_GEARBOX_MOTION_INTERFACE_H__

namespace Zmvs
{

class ZmvsPublicClass VehicleAbstractGearBoxMotionInterface
{
public:
    virtual Real getTorque() const = 0;
	virtual Real getRotation() const = 0;
	virtual void update( Real _deltaTime ) = 0;
	virtual void accelerate( Real _deltaTime ) = 0;
	virtual void deccelerate( Real _deltaTime ) = 0;
    virtual ~VehicleAbstractGearBoxMotionInterface()    {    }
};

}

#endif // __ZMVS_VEHICLE_ABSTRACT_GEARBOX_MOTION_INTERFACE_H__