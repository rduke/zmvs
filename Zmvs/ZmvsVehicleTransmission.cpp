#include "stdafx.h"

using namespace Zmvs;

VehicleTransmission::VehicleTransmission( VehicleTransmissionDsc* _dsc )
    : AbstractObject( _dsc ),
	  MotorizedObject( 0.0f, 0.0f )
{

}

VehicleTransmissionDsc* VehicleTransmission::getDescription() const
{ 
	return static_cast< VehicleTransmissionDsc* >( AbstractObject::getDescription() );
}