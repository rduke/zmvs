#ifndef __ZMVS_VEHICLE_SPECIAL_AUTO_GEAR_BOX_MOTION_H__
#define __ZMVS_VEHICLE_SPECIAL_AUTO_GEAR_BOX_MOTION_H__

#include "ZmvsVehicleSpecialAutoGearBoxMotionDsc.h"
#include "ZmvsVehicleAutoGearBoxMotion.h"
#include "ZmvsVehicleDiscreteGearBox.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleSpecialAutoGearBoxMotion
	: public VehicleAutoGearBoxMotion
{
public:
	VehicleSpecialAutoGearBoxMotion( VehicleSpecialAutoGearBoxMotionDsc* _dsc );
	void forward( Real _deltaTime ); // tmp only
	void backward( Real _deltaTime ); // tmp only
	void update( Real _deltaTime ){}
	VehicleDiscreteGearBox* getGearBox() const;
	VehicleCombustionMotor* getMotor() const;
	void accelerate( Real _deltaTime );
	void deccelerate( Real _deltaTime );
	Real getTorque() const;
	Real getRotation() const;
	const char* getClassName() const;
};

}

#endif // __ZMVS_VEHICLE_SPECIAL_AUTO_GEAR_BOX_MOTION_H__