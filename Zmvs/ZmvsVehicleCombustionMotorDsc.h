#ifndef __ZMVS_VEHICLE_COMBUSTION_MOTOR_DSC_H__
#define __ZMVS_VEHICLE_COMBUSTION_MOTOR_DSC_H__

#include "ZmvsCommon.h"
#include "ZmvsLinearInterpolation.h"
#include "ZmvsVehicleAbstractMotorDsc.h"
#include "ZmvsVehicleMotorDscVisitsAcceptorInterface.h"

namespace Zmvs
{
 
class ZmvsPublicClass VehicleCombustionMotorDsc
	: public VehicleAbstractMotorDsc
{
public:
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_RPM;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_RPM;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_ACCE_SPEED;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_DECCEL_SPEED;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_RPM_TO_GEAR_DOWN;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_RPM_TO_GEAR_UP;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_TORQUE;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MIN_TORQUE;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_SPEED;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_MAX_REAR_SPEED;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_RPM_GAIN_COEEF;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_RPM_LOOSE_COEEF;
	static const Real VEHICLE_COMBUSTION_MOTOR_DEFAULT_STARTING_DELAI;
	static const Real VEHICLE_COMBUSTION_MOTOR_MIN_STARTING_DELAI;

public :
	bool isValid() const;
	void defaultConfig();
	bool print( std::ostream& _out ) const;
	VehicleCombustionMotorDsc* clone() const;
	void accept( VehicleMotorDscVisitorInterface& _visitor );

public :
    Real minRpmToGearDown;
	Real maxRpmToGearUp;
	LinearInterpolationValues torqueCurve;
	
	Real accelDelai;
	Real deccelDelai;
};

}

#endif // __ZMVS_VEHICLE_COMBUSTION_MOTOR_DSC_H__