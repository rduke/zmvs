#include "stdafx.h"

using namespace Zmvs;

VehicleContinuousGearBoxMotionDsc::VehicleContinuousGearBoxMotionDsc()
 : VehicleAbstractGearBoxMotionDsc( new VehicleContinuousGearBoxDsc )
{

}

VehicleContinuousGearBoxMotionDsc* VehicleContinuousGearBoxMotionDsc::clone() const
{
	VehicleContinuousGearBoxMotionDsc* dsc = new VehicleContinuousGearBoxMotionDsc;
	dsc->defaultConfig();
	dsc->motor.reset( motor->clone() );
	dsc->transmission.reset( transmission->clone() );
	dsc->gearBox.reset( static_cast< VehicleAbstractGearBoxDsc* >( gearBox->clone() ) );
	return dsc;
}

const char* VehicleContinuousGearBoxMotionDsc::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleContinuousGearBoxMotionDsc ];
}

void VehicleContinuousGearBoxMotionDsc::accept( VehicleGearBoxMotionDscVisitorInterface &_visitor )
{
    _visitor.visit( *this );
}