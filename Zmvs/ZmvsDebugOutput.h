#ifndef __ZMVS_DEBUG_OUTPUT_H__
#define __ZMVS_DEBUG_OUTPUT_H__

#include "ZmvsCommon.h"
#include <iostream>

namespace Zmvs
{

template < typename DebugObjectType >
class DebugOutput
{
public:
	virtual ~DebugOutput() { }
	virtual bool print( std::ostream& _stream ) const = 0;
	friend std::ostream& operator << ( std::ostream& _stream, const DebugObjectType& _dsc )
	{
        _dsc.print( _stream );
		return _stream;
	}
};

}

#endif //  __ZMVS_DEBUG_OUTPUT_H__