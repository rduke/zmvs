#ifndef __ZMVS_H__
#define __ZMVS_H__

// Zbimir Mechanics Mathematic Simulation Library (only vehicle for the moments)

#include "ZmvsConfig.h"
#include "ZmvsAutoConfig.h"

#include "ZmvsMath.h"
#include "ZmvsClassName.h"

#include "ZmvsSingleton.h"

#include "ZmvsFrictionObject.h"
#include "ZmvsDebugOutput.h"

#include "ZmvsVehiclePhysicModelSimulator.h"

#include "ZmvsVehicleAbstractGearBoxMotion.h"
#include "ZmvsVehicleAbstractGearBoxMotionDsc.h"
#include "ZmvsVehicleAbstractGearBoxMotionInterface.h"

#include "ZmvsVehicleGearBoxMotionDscVisitorInterface.h"
#include "ZmvsVehicleGearBoxMotionDscVisitsAcceptorInterface.h"

#include "ZmvsVehicleSpecialAutoGearBoxMotionDsc.h"
#include "ZmvsVehicleSpecialAutoGearBoxMotion.h"

#include "ZmvsVehicleAutoGearBoxMotionDsc.h"
#include "ZmvsVehicleAutoGearBoxMotion.h"

#include "ZmvsVehicleManualGearBoxMotionDsc.h"
#include "ZmvsVehicleManualGearBoxMotion.h"

#include "ZmvsMotorizedObject.h"

#include "ZmvsVehicleDiscreteGearBox.h"
#include "ZmvsVehicleDiscreteGearBoxDsc.h"

#include "ZmvsVehicleTransmission.h"
#include "ZmvsVehicleTransmissionDsc.h"
#include "ZmvsVehicleTransmissionDscVisitorInterface.h"
#include "ZmvsVehicleTransmissionDscVisitsAcceptorInterface.h"

#include "ZmvsVehicleAbstractSteeringDsc.h"
#include "ZmvsVehicleAbstractSteering.h"
#include "ZmvsVehicleAdaptativeSteeringDsc.h"
#include "ZmvsVehicleAdaptativeSteering.h"
#include "ZmvsVehicleSimpleSteeringDsc.h"
#include "ZmvsVehicleSimpleSteering.h"
#include "ZmvsVehicleSteeringDscVisitorInterface.h"
#include "ZmvsVehicleSteeringDscVisitsAcceptorInterface.h"

#include "ZmvsVehicleAbstractGearBoxDsc.h"
#include "ZmvsVehicleAbstractGearBox.h"
#include "ZmvsVehicleContinuousGearBoxDsc.h"
#include "ZmvsVehicleContinuousGearBox.h"
#include "ZmvsVehicleGearBoxDscVisitorInterface.h"
#include "ZmvsVehicleGearBoxDscVisitsAcceptorInterface.h"

#include "ZmvsVehicleContinuousGearBoxMotionDsc.h"
#include "ZmvsVehicleContinuousGearBoxMotion.h"

#include "ZmvsLinearInterpolation.h"

#include "ZmvsVehicleAbstractMotorDsc.h"
#include "ZmvsVehicleAbstractMotor.h"

#include "ZmvsVehicleCombustionMotorDsc.h"
#include "ZmvsVehicleCombustionMotor.h"

#include "ZmvsVehicleMotorDscVisitorInterface.h"
#include "ZmvsVehicleMotorDscVisitsAcceptorInterface.h"

#include "ZmvsSDKFactory.h"

#endif // __ZMVS_H__