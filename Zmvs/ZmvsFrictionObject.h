#ifndef __ZMVS_FRICTION_OBJECT_H__
#define __ZMVS_FRICTION_OBJECT_H__

namespace Zmvs
{

class FrictionObject
{
public:
	FrictionObject( Real _f = 0 );
	virtual Real getFriction() const;
	virtual void setFriction( Real _f );
	virtual void addFriction( Real _f );
	virtual ~FrictionObject() { }

private:
	Real m_Friction;
};

inline FrictionObject::FrictionObject( Real _f )
: m_Friction( _f )
{

}

inline Real FrictionObject::getFriction() const
{
    return m_Friction;
}

inline void FrictionObject::setFriction( Real _f )
{
    m_Friction = _f;
}

inline void FrictionObject::addFriction( Real _f )
{
    m_Friction += _f;
}

}

#endif // __ZMVS_FRICTION_OBJECT_H__