#ifndef __ZMVS_VEHICLE_ADAPTATIVE_STEERING_DSC_H__
#define __ZMVS_VEHICLE_ADAPTATIVE_STEERING_DSC_H__

#include "ZmvsVehicleAbstractSteeringDsc.h"

namespace Zmvs
{

class VehicleAdaptativeSteeringDsc
	: public VehicleAbstractSteeringDsc
{
public:
	void defaultConfig();
	bool isValid() const;
	void accept( VehicleSteeringDscVisitorInterface& _visitor );
	VehicleAdaptativeSteeringDsc* clone() const;

public:
	Real adaptativeCoeff;
};

}

#endif // __ZMVS_VEHICLE_ADAPTATIVE_STEERING_DSC_H__