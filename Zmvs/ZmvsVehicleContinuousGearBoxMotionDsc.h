#ifndef __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_MOTION_DSC__
#define __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_MOTION_DSC__

#include "ZmvsVehicleAbstractGearBoxMotionDsc.h"
#include "ZmvsVehicleContinuousGearBoxDsc.h"

namespace Zmvs
{

class VehicleContinuousGearBoxMotionDsc
	:public VehicleAbstractGearBoxMotionDsc
{
public:
	VehicleContinuousGearBoxMotionDsc();
    const char* getClassName() const;
	VehicleContinuousGearBoxMotionDsc* clone() const;
	void accept( VehicleGearBoxMotionDscVisitorInterface& _visitor );
};

}

#endif // __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_MOTION_DSC__