#ifndef __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_H__
#define __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_H__

#include "ZmvsAbstractObject.h"
#include "ZmvsVehicleAbstractGearBoxDsc.h"
#include "ZmvsVehicleGearBoxDscVisitsAcceptorInterface.h"

namespace Zmvs
{

class VehicleAbstractGearBox
	: public AbstractObject
{
public:
	VehicleAbstractGearBox( VehicleAbstractGearBoxDsc* _dsc );
	VehicleAbstractGearBoxDsc* getDescription() const;
	virtual void update( Real _deltaTime ) = 0;
	virtual void up( Real _deltaTime ) = 0;
	virtual void down( Real _deltaTime ) = 0;
	virtual Real getRatio() const = 0;
	virtual bool isNeutral() const = 0;
	virtual bool isLocked() const;
	virtual bool setSwitchDelai( Real _delai ) = 0;
	virtual Real getDelai() const { return m_CurrentDelai; }
	virtual ~VehicleAbstractGearBox() { }
	enum GearBoxLogicalStates
	{
        GEAR_SWITCHING_UP,
		GEAR_SWITCHING_DOWN,
		GEAR_READY_TO_SWITCH,
		GEAR_SWITCHED
	};
    virtual GearBoxLogicalStates getLogicalState() const;
protected:
	Real m_CurrentDelai;
	GearBoxLogicalStates m_LogicalState;
};

}

#endif // __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_H__