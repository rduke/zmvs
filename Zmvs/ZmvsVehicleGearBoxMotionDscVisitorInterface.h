#ifndef __ZMVS_VEHICLE_GEARBOX_MOTION_DSC_VISITOR_INTERFACE_H__
#define __ZMVS_VEHICLE_GEARBOX_MOTION_DSC_VISITOR_INTERFACE_H__

namespace Zmvs
{

class VehicleContinuousGearBoxMotionDsc;
class VehicleManualGearBoxMotionDsc;
class VehicleAutoGearBoxMotionDsc;
class VehicleSpecialAutoGearBoxMotionDsc;

class VehicleGearBoxMotionDscVisitorInterface
{
public:
	virtual void visit( VehicleContinuousGearBoxMotionDsc& _dsc ) = 0;
	virtual void visit( VehicleManualGearBoxMotionDsc& _dsc ) = 0;
	virtual void visit( VehicleAutoGearBoxMotionDsc& _dsc ) = 0;
	virtual void visit( VehicleSpecialAutoGearBoxMotionDsc& _dsc ) = 0;
	virtual ~VehicleGearBoxMotionDscVisitorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_GEARBOX_MOTION_DSC_VISITOR_INTERFACE_H__