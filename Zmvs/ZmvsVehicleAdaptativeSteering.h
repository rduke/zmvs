#ifndef __ZMVS_VEHICLE_ADAPTATIVE_STEERING_H__
#define __ZMVS_VEHICLE_ADAPTATIVE_STEERING_H__

#include "ZmvsVehicleAbstractSteering.h"
#include "ZmvsVehicleAdaptativeSteeringDsc.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleAdaptativeSteering
	: public VehicleAbstractSteering
{
public:
	VehicleAdaptativeSteering( VehicleAdaptativeSteeringDsc* _dsc );
	VehicleAdaptativeSteeringDsc* getDescription() const;
	void left( Real _deltaTime );
	void right( Real _deltaTime );
	void update( Real _deltaTime );
	bool setAngle( Real _angle );
	bool setSpeed( Real _value );
	bool setBackSpeed( Real _value );
	void maxLeft();
	void maxRight();
	void straight();
	void notify( Real _chassisSpeed );
	void inverse();

private:
	Real m_ChassisSpeed;
};

}

#endif // __ZMVS_VEHICLE_ADAPTATIVE_STEERING_H__