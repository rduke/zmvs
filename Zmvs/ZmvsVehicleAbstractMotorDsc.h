#ifndef __ZMVS_VEHICLE_ABSTRACT_MOTOR_DSC_H__
#define __ZMVS_VEHICLE_ABSTRACT_MOTOR_DSC_H__

#include "ZmvsAbstractObjectDescription.h"
#include "ZmvsVehicleMotorDscVisitsAcceptorInterface.h"

namespace Zmvs
{

class VehicleAbstractMotorDsc
	: public AbstractObjectDescription,
	  public VehicleMotorDscVisitsAcceptorInterface,
	  public DebugOutput< VehicleAbstractMotorDsc >
{
public:
	Real maxRpm;
	Real minRpm;
};

}

#endif // __ZMVS_VEHICLE_ABSTRACT_MOTOR_DSC_H__