#include "stdafx.h"

using namespace Zmvs;

VehicleManualGearBoxMotionDsc::VehicleManualGearBoxMotionDsc()
: VehicleAbstractGearBoxMotionDsc( new VehicleDiscreteGearBoxDsc )
{

}

const char* VehicleManualGearBoxMotionDsc::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleManualGearBoxMotionDsc ];
}

VehicleManualGearBoxMotionDsc* VehicleManualGearBoxMotionDsc::clone() const 
{
	VehicleManualGearBoxMotionDsc* dsc = new VehicleManualGearBoxMotionDsc;
	dsc->defaultConfig();
	dsc->motor.reset( motor->clone() );
	dsc->transmission.reset( transmission->clone() );
	dsc->gearBox.reset( static_cast< VehicleAbstractGearBoxDsc* >( gearBox->clone() ) );
	return dsc;
}

void VehicleManualGearBoxMotionDsc::accept( VehicleGearBoxMotionDscVisitorInterface &_visitor )
{
    _visitor.visit( *this );
}