#include "stdafx.h"

using namespace Zmvs;

Real VehicleCombustionMotor::getTorque() const
{
	return Math::normalize( getDescription()->torqueCurve.getValue( m_Rpm ), Math::AfterComa_Precision_0 );
}

Real VehicleCombustionMotor::getRotation() const
{
	return Math::normalize( m_Rpm / SIXTY_SECONDS, Math::AfterComa_Precision_0 );
}

void VehicleCombustionMotor::accelerate( Real _deltaTime )
{
   if( m_Rpm + ( ( getDescription()->maxRpm * _deltaTime ) / getDescription()->accelDelai ) < getDescription()->maxRpm )
       m_Rpm += ( ( getDescription()->maxRpm * _deltaTime ) / getDescription()->accelDelai );
   else
	   m_Rpm = getDescription()->maxRpm;
}

void VehicleCombustionMotor::deccelerate( Real _deltaTime )
{
    if( m_Rpm - ( ( getDescription()->maxRpm * _deltaTime ) / getDescription()->deccelDelai ) > getDescription()->minRpm )
        m_Rpm -= ( ( getDescription()->maxRpm * _deltaTime ) * getDescription()->deccelDelai );
    else
	    m_Rpm = getDescription()->minRpm;
}

void VehicleCombustionMotor::update( Real _deltaTime ) // TO DO _deccel first _accel inside _deccel
{
  /*  if( m_Rpm < _deltaTime * getDescription()->minRpmToGearDown / getDescription()->deccelDelai )
        _accelerate( _deltaTime, ( getDescription()->minRpmToGearDown - m_Rpm ) / getDescription()->deccelDelai );*/
	/*else*/ if( m_Rpm > _deltaTime * getDescription()->minRpmToGearDown / getDescription()->deccelDelai )
        _deccelerate( _deltaTime, ( m_Rpm - getDescription()->minRpmToGearDown ) / getDescription()->deccelDelai );
	else 
		m_Rpm = getDescription()->minRpmToGearDown;
}

void VehicleCombustionMotor::_accelerate( Real _deltaTime, Real _accelSpeed )
{
	if( Math::clamp( m_Rpm + _deltaTime * _accelSpeed,
		             getDescription()->minRpm,
					 getDescription()->maxRpm ) )
	   m_Rpm += ( _deltaTime * _accelSpeed );
   else
	   m_Rpm = getDescription()->maxRpm;
}

void VehicleCombustionMotor::_deccelerate( Real _deltaTime, Real _deccelSpeed )
{
   if( Math::clamp( m_Rpm - _deltaTime * _deccelSpeed,
		             getDescription()->minRpm,
					 getDescription()->maxRpm ) )
	   m_Rpm -= ( _deltaTime * _deccelSpeed );
   else
	   m_Rpm = getDescription()->minRpm;
}

VehicleCombustionMotorDsc* VehicleCombustionMotor::getDescription() const
{
	return static_cast< VehicleCombustionMotorDsc* >( AbstractObject::getDescription() );
}


VehicleCombustionMotor::VehicleCombustionMotor( VehicleCombustionMotorDsc* _dsc )
    : VehicleAbstractMotor( _dsc )
{

}

Real VehicleCombustionMotor::getRpm() const
{
	return Math::normalize( m_Rpm, Math::AfterComa_Precision_0 );
}

void VehicleCombustionMotor::setRpm( Real _rpm )
{
	if( !Math::clamp( _rpm, getDescription()->minRpm, getDescription()->maxRpm ) )
	    return;
	m_Rpm = _rpm;
}