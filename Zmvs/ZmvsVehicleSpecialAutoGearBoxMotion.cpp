#include "stdafx.h"

using namespace Zmvs;

VehicleSpecialAutoGearBoxMotion::
VehicleSpecialAutoGearBoxMotion( VehicleSpecialAutoGearBoxMotionDsc* _dsc )
: VehicleAutoGearBoxMotion( _dsc )
{

}

VehicleCombustionMotor* VehicleSpecialAutoGearBoxMotion::getMotor() const
{
	return static_cast< VehicleCombustionMotor* >( VehicleAbstractGearBoxMotion::getMotor() );
}

void VehicleSpecialAutoGearBoxMotion::forward( Real _deltaTime )
{
	accelerate( _deltaTime );
}

void VehicleSpecialAutoGearBoxMotion::backward( Real _deltaTime )
{
	if( getGearBox()->getGear() == VehicleDiscreteGearBox::NEUTRAL )
	{
		getGearBox()->down(0);
		getMotor()->accelerate( _deltaTime );
	}

	else if( getGearBox()->getGear() == VehicleDiscreteGearBox::REAR )
		getMotor()->accelerate( _deltaTime );

	else
		deccelerate( _deltaTime );
}

void VehicleSpecialAutoGearBoxMotion::accelerate( Real _deltaTime )
{
	if( getGearBox()->getGear() == VehicleDiscreteGearBox::NEUTRAL ||
		( getGearBox()->getGear() > VehicleDiscreteGearBox::NEUTRAL &&
		  getGearBox()->getGear() < getGearBox()->getMaxGear() - 1 &&
		  getMotor()->getRpm() > getMotor()->getDescription()->maxRpmToGearUp ) )
	{
		getGearBox()->up(0);
		getMotor()->setRpm( getMotor()->getRpm() - getMotor()->getRpm() * 0.3f );
	}

	else if( getGearBox()->getGear() == VehicleDiscreteGearBox::REAR &&
		     getMotor()->getRpm() > getMotor()->getDescription()->minRpmToGearDown )
			 getMotor()->deccelerate( _deltaTime );

	else if( getGearBox()->getGear() == VehicleDiscreteGearBox::REAR &&
		     getMotor()->getRpm() < getMotor()->getDescription()->minRpmToGearDown )
			 getGearBox()->up(0);

	else
		getMotor()->accelerate( _deltaTime );
}

void VehicleSpecialAutoGearBoxMotion::deccelerate( Real _deltaTime )
{
	if( getGearBox()->getGear() > VehicleDiscreteGearBox::NEUTRAL &&
		getMotor()->getRpm() < getMotor()->getDescription()->minRpmToGearDown )
	{
		getGearBox()->down(0);
		getMotor()->setRpm( getMotor()->getRpm() + getMotor()->getRpm() * 0.3f );
	}

	else if( getGearBox()->getGear() == VehicleDiscreteGearBox::REAR &&
		     getMotor()->getRpm() < getMotor()->getDescription()->minRpmToGearDown )
			 getGearBox()->up(0);

	else
		getMotor()->deccelerate( _deltaTime );
}


Real VehicleSpecialAutoGearBoxMotion::getTorque() const
{
    return getMotor()->getTorque() * getGearBox()->getRatio();
}

Real VehicleSpecialAutoGearBoxMotion::getRotation() const
{
    return getMotor()->getRotation() * getGearBox()->getRatio();
}

const char* VehicleSpecialAutoGearBoxMotion::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleSpecialAutoGearBoxMotion ];
}

VehicleDiscreteGearBox* VehicleSpecialAutoGearBoxMotion::getGearBox() const
{
	return static_cast< VehicleDiscreteGearBox* >( VehicleAbstractGearBoxMotion::getGearBox() );
}
