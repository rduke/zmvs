#ifndef __ZMVS_COMMON_H__
#define __ZMVS_COMMON_H__

#include "ZmvsAutoConfig.h"
#include <math.h>
#include <utility>

namespace Zmvs
{

static const Real PI = 3.14f;

typedef std::pair< Real, Real > RealsPair;
typedef Real MilliSecond;
typedef Real Second;

static const Real SIXTY_SECONDS = 60.0f;

}

#endif // __ZMVS_COMMON_H__