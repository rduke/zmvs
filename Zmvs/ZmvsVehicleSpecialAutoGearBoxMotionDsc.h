#ifndef __ZMVS_VEHICLE_SPECIAL_AUTO_GEAR_BOX_MOTION_DSC_H__
#define __ZMVS_VEHICLE_SPECIAL_AUTO_GEAR_BOX_MOTION_DSC_H__

#include "ZmvsVehicleDiscreteGearBoxDsc.h"
#include "ZmvsVehicleAutoGearBoxMotion.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleSpecialAutoGearBoxMotionDsc
	: public VehicleAutoGearBoxMotionDsc
{
public:
    const char* getClassName() const;
	VehicleSpecialAutoGearBoxMotionDsc* clone() const;
	void accept( VehicleGearBoxMotionDscVisitorInterface& _visitor );
};

}

#endif // __ZMVS_VEHICLE_SPECIAL_AUTO_GEAR_BOX_MOTION_DSC_H__