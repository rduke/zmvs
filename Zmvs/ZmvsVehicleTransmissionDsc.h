#ifndef __ZMVS_VEHICLE_TRANSMISSION_DSC_H__
#define __ZMVS_VEHICLE_TRANSMISSION_DSC_H__

#include "ZmvsVehicleTransmissionDscVisitsAcceptorInterface.h"
#include "ZmvsAbstractObject.h"

namespace Zmvs
{

const int  VEHICLE_TRANSMISSION_MAX_OBSERVERS = 64;
const int  VEHICLE_TRANSMISSION_DEFAULT_OBSERVERS = 4;
const Real VEHICLE_TRANSMISSION_MAX_SPEED = 500.0f;
const Real VEHICLE_TRANSMISSION_MAX_REAR_SPEED = 100.0f;
const Real VEHICLE_TRANSMISSION_DEFAULT_SPEED = 100.0f;
const Real VEHICLE_TRANSMISSION_DEFAULT_REAR_SPEED = 20.0f;

class ZmvsPublicClass VehicleTransmissionDsc
	: public AbstractObjectDescription,
	  public VehicleTransmissionDscVisitsAcceptorInterface,
	  public DebugOutput< VehicleTransmissionDsc >
{
public:
	void defaultConfig();
    virtual bool isValid() const;
	bool print( std::ostream& _out ) const;
	VehicleTransmissionDsc* clone() const;
	void accept( VehicleTransmissionDscVisitorInterface& _visitor );

public:
	size_t maxObservers;
};

}

#endif // __ZMVS_VEHICLE_TRANSMISSION_DSC_H__
