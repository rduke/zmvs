#include "stdafx.h"

using namespace Zmvs;

void VehicleAdaptativeSteeringDsc::defaultConfig()
{
    adaptativeCoeff = 5.0f;
	VehicleAbstractSteeringDsc::defaultConfig();
}

bool VehicleAdaptativeSteeringDsc::isValid() const
{
    if( adaptativeCoeff < 0.0f ||
		!VehicleAbstractSteeringDsc::isValid() )
		return false;

	return true;
}

void VehicleAdaptativeSteeringDsc::accept( VehicleSteeringDscVisitorInterface& _visitor )
{
	_visitor.visit( *this );
}

VehicleAdaptativeSteeringDsc* VehicleAdaptativeSteeringDsc::clone() const
{
    return new VehicleAdaptativeSteeringDsc( *this );
}