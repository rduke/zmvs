#ifndef __ZMVS_VEHICLE_MOTOR_DSC_VISITS_ACCEPTOR_INTERFACE_H__
#define __ZMVS_VEHICLE_MOTOR_DSC_VISITS_ACCEPTOR_INTERFACE_H__

#include "ZmvsVehicleMotorDscVisitorInterface.h"

namespace Zmvs
{

class VehicleMotorDscVisitsAcceptorInterface
{
public:
	virtual void accept( VehicleMotorDscVisitorInterface& _dsc ) = 0;
	virtual ~VehicleMotorDscVisitsAcceptorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_MOTOR_DSC_VISITS_ACCEPTOR_INTERFACE_H__