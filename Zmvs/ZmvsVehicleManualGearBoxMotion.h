#ifndef __ZMVS_VEHICLE_MANUAL_GEAR_BOX_MOTION_H__
#define __ZMVS_VEHICLE_MANUAL_GEAR_BOX_MOTION_H__

#include "ZmvsVehicleManualGearBoxMotionDsc.h"
#include "ZmvsVehicleDiscreteGearBox.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleManualGearBoxMotion
	: public VehicleAbstractGearBoxMotion
{
public:
	VehicleManualGearBoxMotion( VehicleManualGearBoxMotionDsc* _dsc );
	VehicleDiscreteGearBox* getGearBox() const;
	
	void update( Real _deltaTime );
	void accelerate( Real _deltaTime );
	void deccelerate( Real _deltaTime );
	VehicleCombustionMotor* getMotor() const;
	Real getTorque() const;
	Real getRotation() const;
	virtual bool gearUp();
	virtual bool gearDown();
	virtual bool gearNeutral();
	const char* getClassName() const;
};

}


#endif // __ZMVS_VEHICLE_MANUAL_GEAR_BOX_MOTION_H__