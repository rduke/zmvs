#ifndef __ZMVS_VEHICLE_COMBUSTION_MOTOR_H__
#define __ZMVS_VEHICLE_COMBUSTION_MOTOR_H__

#include "ZmvsVehicleCombustionMotorDsc.h"
#include "ZmvsVehicleAbstractMotor.h"
#include "ZmvsCommon.h"
#include "ZmvsMath.h"

namespace Zmvs
{



class ZmvsPublicClass VehicleCombustionMotor
	: public VehicleAbstractMotor
{
public:
	VehicleCombustionMotor( VehicleCombustionMotorDsc* _dsc );
    void setRpm( Real _rpm );
	Real getRpm() const;
	void accelerate( Real _deltaTime );
	void deccelerate( Real _deltaTime );
	void update( Real _deltaTime );
	Real getTorque() const;
	Real getRotation() const;
	VehicleCombustionMotorDsc* getDescription() const;
	
public:
	void _accelerate( Real _deltaTime, Real _accelSpeed );
	void _deccelerate( Real _deltaTime, Real _deccelSpeed );
};

}  

#endif // __ZMVS_VEHICLE_COMBUSTION_MOTOR_H__