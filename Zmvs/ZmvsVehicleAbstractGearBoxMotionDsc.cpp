#include "stdafx.h"

using namespace Zmvs;

VehicleAbstractGearBoxMotionDsc::VehicleAbstractGearBoxMotionDsc( VehicleAbstractGearBoxDsc* _dsc )
:	motor( new VehicleCombustionMotorDsc ),
	transmission( new VehicleTransmissionDsc ),
	gearBox( _dsc )
{

}

const char* VehicleAbstractGearBoxMotionDsc::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleAbstractGearBoxMotionDsc ];
}

void VehicleAbstractGearBoxMotionDsc::defaultConfig()
{
	motor->defaultConfig();
	transmission->defaultConfig();
	gearBox->defaultConfig();
}

bool VehicleAbstractGearBoxMotionDsc::isValid() const
{
	if( motor.get() && transmission.get() && gearBox.get() )
		return motor->isValid() && transmission->isValid() && gearBox->isValid();
	return false;
}