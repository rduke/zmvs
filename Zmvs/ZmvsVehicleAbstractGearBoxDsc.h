#ifndef __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_DSC__
#define __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_DSC__

#include "ZmvsCommon.h"
#include "ZmvsVehicleGearBoxDscVisitsAcceptorInterface.h"
#include "ZmvsDebugOutput.h"
#include "ZmvsClassName.h"
#include "ZmvsAbstractObjectDescription.h"

namespace Zmvs
{

class VehicleAbstractGearBoxDsc
	: public VehicleGearBoxDscVisitsAcceptorInterface,
	  public DebugOutput< VehicleAbstractGearBoxDsc >,
	  public AbstractObjectDescription
{
public:
	bool print( std::ostream& _stream ) const;
	const char* getClassName() const;

public:
	Real switchDelai;
	bool lock;
};

}

#endif // __ZMVS_VEHICLE_ABSTRACT_GEAR_BOX_DSC__