#include "stdafx.h"

using namespace Zmvs;

VehicleTransmissionDsc* VehicleTransmissionDsc::clone() const
{
	return new VehicleTransmissionDsc( *this );
}

bool VehicleTransmissionDsc::print( std::ostream& _out ) const
{
	_out << "maxObservers = " << maxObservers << std::endl;
	return true;
}

bool VehicleTransmissionDsc::isValid() const
{
	return( maxObservers <= VEHICLE_TRANSMISSION_MAX_OBSERVERS &&
		   maxObservers >= 0 );
}

void VehicleTransmissionDsc::defaultConfig()
{
    maxObservers = VEHICLE_TRANSMISSION_DEFAULT_OBSERVERS;
}

void VehicleTransmissionDsc::accept( VehicleTransmissionDscVisitorInterface& _visitor )
{
    _visitor.visit( *this );
}