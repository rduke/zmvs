#ifndef __ZMVS_VEHICLE_TRANSMISSION_DSC_VISITOR_INTERFACE_H__
#define __ZMVS_VEHICLE_TRANSMISSION_DSC_VISITOR_INTERFACE_H__

namespace Zmvs
{

class VehicleTransmissionDsc;

class VehicleTransmissionDscVisitorInterface
{
public:
	virtual void visit( VehicleTransmissionDsc& _dsc ) = 0;
	virtual ~VehicleTransmissionDscVisitorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_TRANSMISSION_DSC_VISITOR_INTERFACE_H__