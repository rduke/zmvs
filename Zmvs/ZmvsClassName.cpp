#include "stdafx.h"

namespace Zmvs
{

const char* ClassName::ClassNameBuffer[ 500 ] = 
{ 	
	    "Object",
		"ObjectDesc",
		"VehicleGearBoxMotionListenable",
		"VehicleAbstractGearBoxDsc",
        "VehicleDiscreteGearBoxDsc",
		"VehicleContinuousGearBoxDsc",
		"VehicleAbstractGearBox",
		"VehicleDiscreteGearBox",
		"VehicleContinuousGearBox",
		"VehicleAbstractGearBoxMotionDsc",
		"VehicleManualGearBoxMotionDsc",
		"VehicleContinuousGearBoxMotionDsc",
		"VehicleAutoGearBoxMotionDsc",
		"VehicleSpecialAutoGearBoxMotionDsc",
        "VehicleAbstractGearBoxMotion",
		"VehicleManualGearBoxMotion",
		"VehicleContinuousGearBoxMotion",
		"VehicleAutoGearBoxMotion",
		"VehicleSpecialAutoGearBoxMotion"
};

}