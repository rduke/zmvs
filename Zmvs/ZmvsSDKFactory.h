#ifndef __ZMVS_SDK_FACTORY_H__
#define __ZMVS_SDK_FACTORY_H__

#include "ZmvsSingleton.h"
#include "ZmvsVehicleDiscreteGearBox.h"
#include "ZmvsVehicleContinuousGearBox.h"
#include "ZmvsVehicleContinuousGearBoxMotion.h"
#include "ZmvsVehicleAutoGearBoxMotion.h"
#include "ZmvsVehicleSpecialAutoGearBoxMotion.h"
#include "ZmvsVehicleAdaptativeSteering.h"
#include "ZmvsVehicleSimpleSteering.h"
#include "ZmvsVehicleTransmission.h"
#include "ZmvsVehicleGearBoxDscVisitorInterface.h"
#include "ZmvsVehicleGearBoxMotionDscVisitorInterface.h"
#include "ZmvsVehicleTransmissionDscVisitorInterface.h"
#include "ZmvsVehicleSteeringDscVisitorInterface.h"
#include "ZmvsVehicleMotorDscVisitorInterface.h"

namespace Zmvs
{

class SDKFactory
	: public Singleton< SDKFactory >,
	  public VehicleGearBoxDscVisitorInterface,
	  public VehicleGearBoxMotionDscVisitorInterface,
	  public VehicleTransmissionDscVisitorInterface,
	  public VehicleSteeringDscVisitorInterface,
	  public VehicleMotorDscVisitorInterface
{
public:
	VehicleAbstractGearBox* createGearBox( VehicleAbstractGearBoxDsc* _dsc );
	VehicleAbstractGearBoxMotion* createGearBoxMotion( VehicleAbstractGearBoxMotionDsc* _dsc );
	VehicleAbstractMotor* createMotor( VehicleAbstractMotorDsc* _dsc );
	VehicleTransmission* createTransmission( VehicleTransmissionDsc* _dsc );
	VehicleAbstractSteering* createSteering( VehicleAbstractSteeringDsc* _dsc );

public:
	void visit( VehicleContinuousGearBoxDsc& _dsc );
    void visit( VehicleDiscreteGearBoxDsc& _dsc );
	
	void visit( VehicleContinuousGearBoxMotionDsc& _dsc );
	void visit( VehicleManualGearBoxMotionDsc& _dsc );
	void visit( VehicleAutoGearBoxMotionDsc& _dsc );
	void visit( VehicleSpecialAutoGearBoxMotionDsc& _dsc );
	
	void visit( VehicleSimpleSteeringDsc& _dsc );
	void visit( VehicleAdaptativeSteeringDsc& _dsc );

	void visit( VehicleTransmissionDsc& _dsc );

	void visit( VehicleCombustionMotorDsc& _dsc );

private:
	VehicleAbstractGearBox* m_CurrentGearBox;
	VehicleAbstractGearBoxMotion* m_CurrentGearBoxMotion;
	VehicleAbstractSteering* m_CurrentSteering;
	VehicleAbstractMotor* m_CurrentMotor;
	VehicleTransmission* m_CurrentTransmission;
};

}

#endif // __ZMVS_SDK_FACTORY_H__