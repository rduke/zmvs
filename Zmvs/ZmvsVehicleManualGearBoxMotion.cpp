#include "stdafx.h"

using namespace Zmvs;

bool VehicleManualGearBoxMotion::gearUp()
{
	getGearBox()->up(0);
	return true;
}

VehicleCombustionMotor* VehicleManualGearBoxMotion::getMotor() const
{
	return static_cast< VehicleCombustionMotor* >( VehicleAbstractGearBoxMotion::getMotor() );
}

bool VehicleManualGearBoxMotion::gearDown()
{
	getGearBox()->down(0);
	return true;
}

bool VehicleManualGearBoxMotion::gearNeutral()
{
	getGearBox()->setGear( 1 );
	return true;
}

void VehicleManualGearBoxMotion::update( Real _deltaTime )
{
    getGearBox()->update( _deltaTime );
	getMotor()->update( _deltaTime );
	getTransmission()->update( getTorque(), getRotation() );
}

Real VehicleManualGearBoxMotion::getTorque() const
{
    return getMotor()->getTorque() * getGearBox()->getRatio();
}

Real VehicleManualGearBoxMotion::getRotation() const
{
	return getMotor()->getRotation() / getGearBox()->getRatio();
}

const char* VehicleManualGearBoxMotion::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleManualGearBoxMotion ];
}

VehicleManualGearBoxMotion::
VehicleManualGearBoxMotion( VehicleManualGearBoxMotionDsc* _dsc )
: VehicleAbstractGearBoxMotion( _dsc )
{
    gearNeutral();
}

VehicleDiscreteGearBox* VehicleManualGearBoxMotion::getGearBox() const
{
	return static_cast< VehicleDiscreteGearBox* >( VehicleAbstractGearBoxMotion::getGearBox() );
}

void VehicleManualGearBoxMotion::accelerate( Real _deltaTime )
{
	getMotor()->accelerate( _deltaTime );
}

void VehicleManualGearBoxMotion::deccelerate( Real _deltaTime )
{
	getMotor()->deccelerate( _deltaTime );
}
