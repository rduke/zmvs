#include "stdafx.h"

using namespace Zmvs;

bool VehicleAbstractGearBoxDsc::print( std::ostream& _stream ) const
{
	_stream << ClassName::ClassNameBuffer[ ClassName::_VehicleAbstractGearBoxDsc ] << std::endl;
	_stream << "switchDelai = " << switchDelai << std::endl;
	_stream << "lock = " << lock << std::endl;
	return true;
}

const char* VehicleAbstractGearBoxDsc::getClassName() const
{ 
	return ClassName::ClassNameBuffer[_VehicleAbstractGearBoxDsc];
}