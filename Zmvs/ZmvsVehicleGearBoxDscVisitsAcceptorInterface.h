#ifndef __ZMVS_VEHICLE_GEARBOX_DSC_VISITS_ACCEPTOR_INTERFACE_H__
#define __ZMVS_VEHICLE_GEARBOX_DSC_VISITS_ACCEPTOR_INTERFACE_H__

#include "ZmvsVehicleGearBoxDscVisitorInterface.h"

namespace Zmvs
{

class VehicleGearBoxDscVisitsAcceptorInterface
{
public:
	virtual void accept( VehicleGearBoxDscVisitorInterface& _dsc ) = 0;
	virtual ~VehicleGearBoxDscVisitsAcceptorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_GEARBOX_DSC_VISITS_ACCEPTOR_INTERFACE_H__