#include "stdafx.h"

using namespace Zmvs;

const char* VehicleSpecialAutoGearBoxMotionDsc::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleSpecialAutoGearBoxMotionDsc ];
}

VehicleSpecialAutoGearBoxMotionDsc* VehicleSpecialAutoGearBoxMotionDsc::clone() const
{
	return static_cast< VehicleSpecialAutoGearBoxMotionDsc* >( VehicleManualGearBoxMotionDsc::clone() );
}

void VehicleSpecialAutoGearBoxMotionDsc::accept( VehicleGearBoxMotionDscVisitorInterface &_visitor )
{
    _visitor.visit( *this );
}