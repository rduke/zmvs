#include "stdafx.h"

using namespace Zmvs;

const Real VehicleAbstractSteeringDsc::VEHICLE_ABSTRACT_STEERING_MAX_ANGLE = PI / 2.0f;

bool VehicleAbstractSteeringDsc::print( std::ostream& _stream ) const
{
	_stream << "initialAngle = " << initialAngle << std::endl;
	_stream << "maxAngle = " << maxAngle << std::endl;
	_stream << "speed = " << speed << std::endl;
	_stream << "backSpeed = " << backSpeed << std::endl;
	return true;
}

void VehicleAbstractSteeringDsc::defaultConfig()
{
	initialAngle = 0.0f;
	maxAngle = PI / 4.0f;
	speed = 2.0f;
	backSpeed = 1.5f;
}

bool VehicleAbstractSteeringDsc::isValid() const
{
	return !( speed < backSpeed ||
		      maxAngle > VEHICLE_ABSTRACT_STEERING_MAX_ANGLE ||
		      speed < 0.0f ||
			  backSpeed < 0.0f ||
			  maxAngle < 0.0f ||
		      initialAngle < 0.0f );
}