#ifndef __ZMVS_VEHICLE_ABSTRACT_MOTOR_H__
#define __ZMVS_VEHICLE_ABSTRACT_MOTOR_H__

#include "ZmvsAbstractObject.h"
#include "ZmvsMotorizedObject.h"

namespace Zmvs
{

class VehicleAbstractMotor
	: public AbstractObject,
	  public MotorizedObject
{
public:
	static const Real INITIAL_TORQUE;
	static const Real INITIAL_ROTATION;
public:
	VehicleAbstractMotor( VehicleAbstractMotorDsc* _dsc );
	VehicleAbstractMotorDsc* getDescription() const;
	virtual void accelerate( Real _deltaTime ) = 0;
	virtual void update( Real _deltaTime ) = 0;
    virtual void setRpm( Real _rpm );
	virtual Real getRpm() const;
protected:
	Real m_Rpm;
};

}

#endif // __ZMVS_VEHICLE_ABSTRACT_MOTOR_H__