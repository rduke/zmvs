#ifndef __ZMVS_VEHICLE_PHYSIC_MODEL_SIMULATOR_H__
#define __ZMVS_VEHICLE_PHYSIC_MODEL_SIMULATOR_H__

#include "ZmvsCommon.h"

namespace Zmvs
{

class VehiclePhysicModelSimulator
{
public:
	static const Real G_CONSTANT_0_013606;
	static const Real G_CONSTANT_0_00033;
	static const Real G_CONSTANT_8_10e_5;

	void setMass( Real _mass ) { m_Mass = _mass; }
	void setWheelsRadius( Real _wheelsRadius ) { m_WheelsRadius = _wheelsRadius; }
	Real getMass() const { return m_Mass; }
	Real getResistingCouple() const { return m_ResistingCouple; }
	Real getWheelsFriction() const { return m_WheelsFriction; }
	Real getG() const { return m_G; }
	Real getWheelsRadius() const { return m_WheelsRadius; }
	void update( Real _velocity );
	virtual ~VehiclePhysicModelSimulator() { }

protected:
	void calculateG( Real _velocity );
	void calculateWheelsFriction( Real _velocity );
	void calculateResistingCouple();

private:
	Real m_ResistingCouple;
	Real m_WheelsFriction;
	Real m_Mass;
	Real m_G;
	Real m_WheelsRadius;
};

inline void VehiclePhysicModelSimulator::calculateG( Real _velocity )
{
    m_G = G_CONSTANT_0_013606 + G_CONSTANT_0_00033 * _velocity + 
		  G_CONSTANT_8_10e_5 * _velocity * _velocity;
}

inline void VehiclePhysicModelSimulator::calculateWheelsFriction( Real _velocity )
{
    m_WheelsFriction = m_Mass * m_G / 2;
}

inline void VehiclePhysicModelSimulator::calculateResistingCouple()
{
    m_ResistingCouple = m_WheelsFriction * m_WheelsRadius;
}

inline void VehiclePhysicModelSimulator::update( Real _velocity )
{
	calculateG( _velocity );
	calculateWheelsFriction( _velocity );
	calculateResistingCouple();
}

}

#endif // __ZMVS_VEHICLE_PHYSIC_MODEL_SIMULATOR_H__