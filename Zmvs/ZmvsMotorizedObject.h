#ifndef __ZMVS_MOTORIZED_OBJECT_H__
#define __ZMVS_MOTORIZED_OBJECT_H__

#include "ZmvsCommon.h"

namespace Zmvs
{

class ZmvsPublicClass MotorizedObject
{
public:
	MotorizedObject( Real _torque, Real _rotation );
	virtual ~MotorizedObject()    {    }
	virtual void update( Real _torque, Real _rotation );
	Real getTorque() const;
	Real getRotation() const;
	void setTorque( Real _torque );
	void setRotation( Real _rotation );

protected:
	Real m_Torque;
	Real m_Rotation;
};

inline MotorizedObject::MotorizedObject( Real _torque, Real _rotation )
: m_Torque( _torque ),
  m_Rotation( _rotation )
{

}

inline Real MotorizedObject::getTorque() const
{
	return m_Torque;
}

inline Real MotorizedObject::getRotation() const
{
	return m_Rotation;
}

inline void MotorizedObject::setTorque( Real _torque )
{
    m_Torque = _torque;
}

inline void MotorizedObject::setRotation( Real _rotation )
{
    m_Rotation = _rotation;
}

inline void MotorizedObject::update( Real _torque, Real _rotation )
{
	m_Torque = _torque;
	m_Rotation = _rotation;
}

}

#endif // __ZMVS_MOTORIZED_OBJECT_H__