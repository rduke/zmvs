#include "stdafx.h"

using namespace Zmvs;

const Real VehicleDiscreteGearBoxDsc::DEFAULT_GEAR_SWITCH_DELAI = 1.0f;
const Real VehicleDiscreteGearBoxDsc::RATIONAL_GEAR_SWITCH_MIN_DELAI = 0.050f;
const Real VehicleDiscreteGearBoxDsc::RATIONAL_GEAR_SWITCH_MAX_DELAI = 10.0f;

void VehicleDiscreteGearBoxDsc::defaultConfig()
{
	gearsRatio.clear();
	// rear ratio
	gearsRatio.push_back( -2.8f );
	// neutral ratio
	gearsRatio.push_back( 0.0f );
	// forward ratio
    gearsRatio.push_back( 2.66f );
    gearsRatio.push_back( 2.00f );
    gearsRatio.push_back( 1.50f );
    gearsRatio.push_back( 1.00f );
    gearsRatio.push_back( 0.74f );
    gearsRatio.push_back( 0.50f );

	lock = false;

	switchDelai = DEFAULT_GEAR_SWITCH_DELAI;
}

bool VehicleDiscreteGearBoxDsc::isValid() const
{
	if( gearsRatio.size() < 3       ||
		gearsRatio[ 0 ] >  0.0f &&
		gearsRatio[ 1 ] != 0.0f )
		return false;

	for( GearsRatioConstIterator it = gearsRatio.begin() + 2;
		 it != gearsRatio.end() - 1;
		 ++it )
       if( ( *it ) < ( *( it + 1 ) ) )
		   return false;

	return true;
}

bool VehicleDiscreteGearBoxDsc::print( std::ostream& _stream ) const
{
	if( !VehicleAbstractGearBoxDsc::print( _stream ) )
		return false;
    
	_stream << getClassName() << std::endl;

	for( size_t i = 0; i < gearsRatio.size(); i++ )
         _stream << "gearsRatio[" << i << "] = " << gearsRatio[ i ] << std::endl;
	return true;
}

const char* VehicleDiscreteGearBoxDsc::getClassName() const
{
	return ClassName::ClassNameBuffer[ ClassName::_VehicleDiscreteGearBoxDsc ];
}

VehicleDiscreteGearBoxDsc* VehicleDiscreteGearBoxDsc::clone() const
{ 
	return new VehicleDiscreteGearBoxDsc( *this );
}

void VehicleDiscreteGearBoxDsc::accept( VehicleGearBoxDscVisitorInterface& _visitor )
{
	_visitor.visit( *this );
}