#ifndef __ZMVS_VEHICLE_TRANSMISSION_H__
#define __ZMVS_VEHICLE_TRANSMISSION_H__

#include "ZmvsMotorizedObject.h"
#include "ZmvsVehicleTransmissionDsc.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleTransmission
	: public MotorizedObject,
	  public AbstractObject
{
public:
	VehicleTransmission( VehicleTransmissionDsc* _dsc );
	void notify();
	VehicleTransmissionDsc* getDescription() const;
};

}

#endif // __ZMVS_VEHICLE_TRANSMISSION_H__