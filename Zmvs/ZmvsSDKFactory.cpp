#include "stdafx.h"

using namespace Zmvs;

void SDKFactory::visit( VehicleContinuousGearBoxDsc& _dsc )
{
    m_CurrentGearBox = new VehicleContinuousGearBox( &_dsc );
}

void SDKFactory::visit( VehicleDiscreteGearBoxDsc& _dsc )
{
    m_CurrentGearBox = new VehicleDiscreteGearBox( &_dsc );
}

void SDKFactory::visit( VehicleContinuousGearBoxMotionDsc& _dsc )
{
    m_CurrentGearBoxMotion = new VehicleContinuousGearBoxMotion( &_dsc );
}

void SDKFactory::visit( VehicleManualGearBoxMotionDsc& _dsc )
{
    m_CurrentGearBoxMotion = new VehicleManualGearBoxMotion( &_dsc );
}

void SDKFactory::visit( VehicleAutoGearBoxMotionDsc& _dsc )
{
    m_CurrentGearBoxMotion = new VehicleAutoGearBoxMotion( &_dsc );
}

void SDKFactory::visit( VehicleSpecialAutoGearBoxMotionDsc& _dsc )
{
    m_CurrentGearBoxMotion = new VehicleSpecialAutoGearBoxMotion( &_dsc );
}

void SDKFactory::visit( VehicleTransmissionDsc& _dsc )
{
    m_CurrentTransmission = new VehicleTransmission( &_dsc );
}

void SDKFactory::visit( VehicleCombustionMotorDsc& _dsc )
{
    m_CurrentMotor = new VehicleCombustionMotor( &_dsc );
}

void SDKFactory::visit( VehicleSimpleSteeringDsc& _dsc )
{
    m_CurrentSteering = new VehicleSimpleSteering( &_dsc );
}

void SDKFactory::visit( VehicleAdaptativeSteeringDsc& _dsc )
{
    m_CurrentSteering = new VehicleAdaptativeSteering( &_dsc );
}



VehicleAbstractGearBox* SDKFactory::createGearBox( VehicleAbstractGearBoxDsc* _dsc )
{
	_dsc->accept( *this );
	return m_CurrentGearBox;
}

VehicleAbstractGearBoxMotion* SDKFactory::createGearBoxMotion( VehicleAbstractGearBoxMotionDsc* _dsc )
{
	_dsc->accept( *this );
	return m_CurrentGearBoxMotion;
}

VehicleAbstractMotor* SDKFactory::createMotor( VehicleAbstractMotorDsc* _dsc )
{
	_dsc->accept( *this );
	return m_CurrentMotor;
}

VehicleTransmission* SDKFactory::createTransmission( VehicleTransmissionDsc* _dsc )
{
	_dsc->accept( *this );
	return m_CurrentTransmission;
}

VehicleAbstractSteering* SDKFactory::createSteering( VehicleAbstractSteeringDsc* _dsc )
{
	_dsc->accept( *this );
	return m_CurrentSteering;
}