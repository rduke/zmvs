#include "stdafx.h"

using namespace Zmvs;

VehicleSimpleSteeringDsc* VehicleSimpleSteeringDsc::clone() const
{
    return new VehicleSimpleSteeringDsc( *this );
}

void VehicleSimpleSteeringDsc::accept( VehicleSteeringDscVisitorInterface& _visitor )
{
	_visitor.visit( *this );
}