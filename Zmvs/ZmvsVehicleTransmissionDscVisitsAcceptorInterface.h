#ifndef __ZMVS_VEHICLE_TRANSMISSION_DSC_VISITS_ACCEPTOR_INTERFACE_H__
#define __ZMVS_VEHICLE_TRANSMISSION_DSC_VISITS_ACCEPTOR_INTERFACE_H__

#include "ZmvsVehicleTransmissionDscVisitorInterface.h"

namespace Zmvs
{

class VehicleTransmissionDsc;

class VehicleTransmissionDscVisitsAcceptorInterface
{
public:
	virtual void accept( VehicleTransmissionDscVisitorInterface& _dsc ) = 0;
	virtual ~VehicleTransmissionDscVisitsAcceptorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_TRANSMISSION_DSC_VISITS_ACCEPTOR_INTERFACE_H__