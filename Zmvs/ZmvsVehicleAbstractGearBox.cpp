#include "stdafx.h"

using namespace Zmvs;

VehicleAbstractGearBox::VehicleAbstractGearBox( VehicleAbstractGearBoxDsc* _dsc )
: AbstractObject( _dsc ),
  m_CurrentDelai( 0.0f ),
  m_LogicalState( GEAR_SWITCHED )
{
}

VehicleAbstractGearBoxDsc* VehicleAbstractGearBox::getDescription() const
{ 
	return static_cast< VehicleAbstractGearBoxDsc* >( AbstractObject::getDescription() );
}

bool VehicleAbstractGearBox::isLocked() const
{
	return getDescription()->lock;
}

VehicleAbstractGearBox::GearBoxLogicalStates 
VehicleAbstractGearBox::getLogicalState() const
{
	return m_LogicalState;

}