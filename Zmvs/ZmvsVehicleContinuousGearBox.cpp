#include "stdafx.h"

using namespace Zmvs;

VehicleContinuousGearBox::VehicleContinuousGearBox( VehicleContinuousGearBoxDsc* _dsc )
: VehicleAbstractGearBox( _dsc ),
  m_Ratio( _dsc->maxRatio ),
  m_State( VEHICLE_CONTINUOUS_GEAR_BOX_NEUTRAL ),
  m_MotorRpm( 0.0f )
{
	m_LogicalState = GEAR_SWITCHED;
}

void VehicleContinuousGearBox::setMotorRpm( Real _rpm )
{
    m_MotorRpm = _rpm;
}

bool VehicleContinuousGearBox::setSwitchDelai( Real _delai )
{
	if( !Math::clamp( _delai,    VehicleContinuousGearBoxDsc::RATIONAL_GEAR_SWITCH_MIN_DELAI,
		                   VehicleContinuousGearBoxDsc::RATIONAL_GEAR_SWITCH_MAX_DELAI ) )
	    return false;

	getDescription()->switchDelai = _delai;
	return true;
}

void VehicleContinuousGearBox::update( Real _deltaTime )
{
	if( m_MotorRpm < getDescription()->motorRpm )
			up( _deltaTime );
	else if ( m_MotorRpm > getDescription()->motorRpm )
			down( _deltaTime );

	if( m_LogicalState != GEAR_SWITCHING_UP &&
	    m_LogicalState != GEAR_SWITCHING_DOWN )
	    return;

	if( m_CurrentDelai < getDescription()->switchDelai )
			m_CurrentDelai += _deltaTime;
	else 
	{
		if( m_MotorRpm < getDescription()->motorRpm )
			if( m_Ratio + getDescription()->discretisationCoeff * getDescription()->maxRatio > getDescription()->maxRatio )
				m_Ratio = getDescription()->maxRatio;
			else
				m_Ratio += getDescription()->discretisationCoeff * getDescription()->maxRatio;

		else if ( m_MotorRpm > getDescription()->motorRpm )
			if( m_Ratio - getDescription()->discretisationCoeff * getDescription()->maxRatio < getDescription()->minRatio )
				m_Ratio = getDescription()->minRatio;
			else
				m_Ratio -= getDescription()->discretisationCoeff * getDescription()->maxRatio;

		m_LogicalState = GEAR_SWITCHED;
		m_CurrentDelai = 0.0f;
	}
}

void VehicleContinuousGearBox::up( Real _deltaTime )
{
	if( isLocked() || m_LogicalState == GEAR_SWITCHING_UP )
		return;
    
	m_LogicalState = GEAR_SWITCHING_UP;
}

void VehicleContinuousGearBox::down( Real _deltaTime )
{
	if( isLocked() || m_LogicalState == GEAR_SWITCHING_DOWN )
		return;
    
	m_LogicalState = GEAR_SWITCHING_DOWN;
}

bool VehicleContinuousGearBox::isNeutral() const
{
	return ( m_Ratio == 0.0f );
}

VehicleContinuousGearBoxDsc* VehicleContinuousGearBox::getDescription() const
{ 
	return static_cast< VehicleContinuousGearBoxDsc* >( VehicleAbstractGearBox::getDescription() );
}

Real VehicleContinuousGearBox::getRatio() const
{
	return m_Ratio;
}

VehicleContinuousGearBox::VehicleContinuousGearBoxStates
VehicleContinuousGearBox::getState() const
{
    return m_State;
}