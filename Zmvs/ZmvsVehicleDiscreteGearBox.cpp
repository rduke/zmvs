#include "stdafx.h"

using namespace Zmvs;

const Real VehicleDiscreteGearBox::GEAR_OUT_OF_RANGE = -11111.0f;

bool VehicleDiscreteGearBox::setSwitchDelai( Real _delai )
{
	if( !Math::clamp( _delai, VehicleDiscreteGearBoxDsc::RATIONAL_GEAR_SWITCH_MIN_DELAI,
		                   VehicleDiscreteGearBoxDsc::RATIONAL_GEAR_SWITCH_MAX_DELAI ) )
	    return false;

	getDescription()->switchDelai = _delai;
	return true;
}

bool VehicleDiscreteGearBox::setGear( size_t _currGear )
{
	if( _currGear < 0 ||
		_currGear > getDescription()->gearsRatio.size() - 1 ||
		isLocked() ||
		m_LogicalState == GEAR_SWITCHING_UP ||
		m_LogicalState == GEAR_SWITCHING_DOWN )
        return false;

	m_Gear = _currGear;
	return true;
}

Real VehicleDiscreteGearBox::getRatio( size_t _gear ) const
{
	if( _gear < 0 ||
		_gear > getDescription()->gearsRatio.size() - 1 )
    return GEAR_OUT_OF_RANGE;
#if defined( ZMVS_DEBUG )
	return getDescription()->gearsRatio.at( _gear );
#else
	return getDescription()->gearsRatio[ _gear ];
#endif
}

void VehicleDiscreteGearBox::up( Real _deltaTime )
{
	if( isLocked() || 
		m_LogicalState == GEAR_SWITCHING_UP ||
		m_LogicalState == GEAR_SWITCHING_DOWN ||
		m_Gear >= getDescription()->gearsRatio.size() - 1 )
		return;

	m_LogicalState = GEAR_SWITCHING_UP;
}

void VehicleDiscreteGearBox::update( Real _deltaTime )
{
    if( m_LogicalState != GEAR_SWITCHING_UP &&
		m_LogicalState != GEAR_SWITCHING_DOWN )
		return;

	if( m_CurrentDelai + _deltaTime < getDescription()->switchDelai )
			m_CurrentDelai += _deltaTime;
	else 
	{
		if( m_LogicalState == GEAR_SWITCHING_UP )
			m_Gear++;
		else if( m_LogicalState == GEAR_SWITCHING_DOWN )
			m_Gear--;

        m_CurrentDelai = 0.0f;
		m_LogicalState = GEAR_SWITCHED;
	}
}

void VehicleDiscreteGearBox::down( Real _deltaTime )
{
	if( isLocked() ||
		m_LogicalState == GEAR_SWITCHING_UP ||
		m_LogicalState == GEAR_SWITCHING_DOWN ||
		!m_Gear )
		return;

	m_LogicalState = GEAR_SWITCHING_DOWN;
}

bool VehicleDiscreteGearBox::isNeutral() const
{
	return m_Gear == VehicleDiscreteGearBox::NEUTRAL;
}

VehicleDiscreteGearBoxDsc* VehicleDiscreteGearBox::getDescription() const
{ 
	return static_cast< VehicleDiscreteGearBoxDsc* >( VehicleAbstractGearBox::getDescription() );
}

VehicleDiscreteGearBox::VehicleDiscreteGearBox(VehicleDiscreteGearBoxDsc* _dsc )
: VehicleAbstractGearBox( _dsc )
{
	m_Gear = NEUTRAL;
	m_LogicalState = GEAR_SWITCHED;
}

size_t VehicleDiscreteGearBox::getGear() const
{
	return m_Gear;
}

size_t VehicleDiscreteGearBox::getMaxGear() const
{
	return getDescription()->gearsRatio.size() - 1;
}


Real VehicleDiscreteGearBox::getRatio() const
{
#if defined( ZMVS_DEBUG )
	return getDescription()->gearsRatio.at( m_Gear );
#else
    return getDescription()->gearsRatio[ m_Gear ];
#endif
}
