#ifndef __ZMVS_VEHICLE_STEERING_ABSTRACT_INTERFACE_H__
#define __ZMVS_VEHICLE_STEERING_ABSTRACT_INTERFACE_H__

#include "ZmvsAbstractObject.h"
#include "ZmvsVehicleAbstractSteeringDsc.h"

namespace Zmvs
{

class ZmvsPublicClass VehicleAbstractSteering
	: public AbstractObject
{
public:
	VehicleAbstractSteering( VehicleAbstractSteeringDsc* _dsc );
	virtual void left( Real _deltaTime ) = 0;
	virtual void right( Real _deltaTime ) = 0;
	virtual void update( Real _deltaTime ) = 0;
	virtual bool setAngle( Real _angle );
	virtual bool setSpeed( Real _value ) = 0;
	virtual bool setBackSpeed( Real _value ) = 0;
	Real getAngle() const;
	virtual void maxLeft() = 0;
	virtual void maxRight() = 0;
	virtual void straight() = 0;
	virtual void inverse() = 0;
	virtual ~VehicleAbstractSteering() {}
private:
	Real m_Angle;
};

}

#endif // __ZMVS_VEHICLE_STEERING_ABSTRACT_INTERFACE_H__