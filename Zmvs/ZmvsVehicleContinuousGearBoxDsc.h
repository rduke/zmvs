#ifndef __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_DSC_H__
#define __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_DSC_H__

#include "ZmvsCommon.h"
#include "ZmvsVehicleAbstractGearBoxDsc.h"

namespace Zmvs
{

class VehicleContinuousGearBoxDsc
	: public VehicleAbstractGearBoxDsc
{
public:
	bool isValid() const;
	VehicleContinuousGearBoxDsc* clone() const;
    void defaultConfig();
	void accept( VehicleGearBoxDscVisitorInterface& _visitor );

public:
	static const Real RATIO_INTERVAL_RATIONAL_LOWER_LIMIT;
    static const Real RATIO_INTERVAL_RATIONAL_UPPER_LIMIT;
    static const Real DEFAULT_MOTOR_RPM;
	static const Real RATIONAL_GEAR_SWITCH_MIN_DELAI;
	static const Real RATIONAL_GEAR_SWITCH_MAX_DELAI;
	static const Real DEFAULT_SWITCH_DELAI;
	static const Real DEFAULT_DISCRETISATION_COEFF;
	static const Real DEFAULT_MIN_DISCRETISATION_COEFF;
	static const Real DEFAULT_MAX_DISCRETISATION_COEFF;

public:
	Real            minRatio;
	Real            maxRatio;
	Real            minRearRatio;
	Real            maxRearRatio;
	Real            motorRpm;
	Real            discretisationCoeff;
};

}

#endif // __ZMVS_VEHICLE_CONTINUOUS_GEAR_BOX_DSC_H__