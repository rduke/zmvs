#ifndef __ZMVS_VEHICLE_STEERING_DSC_VISITS_ACCEPTOR_INTERFACE_H__
#define __ZMVS_VEHICLE_STEERING_DSC_VISITS_ACCEPTOR_INTERFACE_H__

#include "ZmvsVehicleSteeringDscVisitorInterface.h"

namespace Zmvs
{

class VehicleSteeringDscVisitsAcceptorInterface
{
public:
	virtual void accept( VehicleSteeringDscVisitorInterface& _dsc ) = 0;
	virtual ~VehicleSteeringDscVisitsAcceptorInterface() { }
};

}

#endif // __ZMVS_VEHICLE_STEERING_DSC_VISITS_ACCEPTOR_INTERFACE_H__