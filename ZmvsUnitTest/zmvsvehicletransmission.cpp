#include "module.h"

BOOST_AUTO_TEST_SUITE( TransmissionTest )

BOOST_AUTO_TEST_CASE( Transmission )
{
	std::cout << "********** Transmission test begin **********" << std::endl;
	std::auto_ptr<Zmvs::VehicleTransmissionDsc> tr_dsc(new Zmvs::VehicleTransmissionDsc);
	BOOST_CHECK( !tr_dsc->isValid() );
	tr_dsc->defaultConfig();
	BOOST_CHECK( tr_dsc->isValid() );
	std::cout << *( tr_dsc.get() ) << std::endl;

	std::auto_ptr< Zmvs::VehicleTransmission > tr( Zmvs::SDKFactory::getSingleton()->createTransmission( tr_dsc.get() ) );
	std::cout << *( tr->getDescription() ) << std::endl;
	BOOST_CHECK( Zmvs::Math::isZero( tr->getRotation() ) );
	std::cout << "rotation = " << tr->getRotation() << std::endl;
	std::cout << "torque = "   << tr->getTorque() << std::endl;

	BOOST_CHECK( Zmvs::Math::isZero( tr->getTorque() ) );
	std::cout << "********** Transmission test end **********" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()