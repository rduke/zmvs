#include "module.h"

BOOST_AUTO_TEST_SUITE( SteeringTest )

BOOST_AUTO_TEST_CASE( SimpleSteering )
{
	std::cout << "********** Simple steering test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleSimpleSteeringDsc > steer_dsc( new Zmvs::VehicleSimpleSteeringDsc );
	BOOST_CHECK( !steer_dsc->isValid() );
	steer_dsc->defaultConfig();
	BOOST_CHECK( steer_dsc->isValid() );
	std::cout << *( steer_dsc.get() ) << std::endl;

	std::auto_ptr< Zmvs::VehicleSimpleSteering > steer( static_cast< Zmvs::VehicleSimpleSteering* >( Zmvs::SDKFactory::getSingleton()->createSteering( steer_dsc.get() ) ) );
	steer->right( 1000.0f );
	BOOST_CHECK( steer->getAngle() < 0.0f );
	std::cout << "right angle = " << steer->getAngle() << std::endl;
	steer->left( 1000.0f );
	BOOST_CHECK( steer->getAngle() > 0.0f );
	std::cout << "left angle = " << steer->getAngle() << std::endl;
	Zmvs::Real prevAngle = steer->getAngle();
	steer->update( 1.0f );
	BOOST_CHECK( prevAngle > steer->getAngle() );
	std::cout << "prev angle > current angle = " << prevAngle << " " << steer->getAngle() << std::endl;
	steer->straight();
	BOOST_CHECK( Zmvs::Math::clamp( steer->getAngle(), -0.1f, 0.1f ) );
	std::cout << "straight angle = " << steer->getAngle() << std::endl;
	std::cout << "********** Simple steering test end **********" << std::endl;
}

BOOST_AUTO_TEST_CASE( AdaptativeSteering )
{
	std::cout << "********** Adaptative steering test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleAdaptativeSteeringDsc > steer_dsc( new Zmvs::VehicleAdaptativeSteeringDsc );
	BOOST_CHECK( !steer_dsc->isValid() );
	steer_dsc->defaultConfig();
	BOOST_CHECK( steer_dsc->isValid() );
	std::cout << *( steer_dsc.get() ) << std::endl;

	std::auto_ptr< Zmvs::VehicleAdaptativeSteering > steer( static_cast< Zmvs::VehicleAdaptativeSteering* >( Zmvs::SDKFactory::getSingleton()->createSteering( steer_dsc.get() ) ) );
	steer->notify( 100.0f );
	steer->right( 1000.0f );
	steer->update( 10.0f );
	BOOST_CHECK( steer->getAngle() < 0.0f );
	std::cout << "right angle = " << steer->getAngle() << std::endl;
	steer->left( 1000.0f );
	BOOST_CHECK( steer->getAngle() > 0.0f );
	std::cout << "left angle = " << steer->getAngle() << std::endl;
	steer->straight();
	std::cout << "straight angle = " << steer->getAngle() << std::endl;
	BOOST_CHECK( Zmvs::Math::clamp( steer->getAngle(), -0.1f, 0.1f ) ); 
	std::cout << "********** Adaptative steering test end **********" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()