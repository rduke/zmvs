#include "module.h"

BOOST_AUTO_TEST_SUITE( MotorTest )

BOOST_AUTO_TEST_CASE( Motor )
{
	std::cout << "********** Combustion motor test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleCombustionMotorDsc > mtr_dsc( new Zmvs::VehicleCombustionMotorDsc );
	BOOST_CHECK( !mtr_dsc->isValid() );
	mtr_dsc->defaultConfig();
	BOOST_CHECK( mtr_dsc->isValid() );
	std::cout << *( mtr_dsc.get() ) << std::endl;

	std::auto_ptr< Zmvs::VehicleCombustionMotor > mtr( static_cast< Zmvs::VehicleCombustionMotor* >( Zmvs::SDKFactory::getSingleton()->createMotor( mtr_dsc.get() ) ) );
	std::cout << *( mtr->getDescription() ) << std::endl;
	mtr->update( 1.0f );
	Zmvs::Real prevRpm = mtr->getRpm();
	Zmvs::Real prevTorque = mtr->getTorque();
	std::cout << "prev rpm = " << prevRpm << std::endl;
	std::cout << "prev torque = " << prevTorque << std::endl;
	BOOST_CHECK( mtr->getRpm() >= mtr->getDescription()->minRpm );
	std::cout << "rpm after update( 1000.0f ) = " << mtr->getRpm() << std::endl;
	BOOST_CHECK( mtr->getTorque() > 0.0f );
	std::cout << "torque = " << mtr->getTorque() << std::endl;
	mtr->accelerate( 10.0f );
	BOOST_CHECK( mtr->getRpm() > prevRpm );
	BOOST_CHECK( mtr->getTorque() <= prevTorque );
	BOOST_CHECK( Zmvs::Math::clamp( mtr->getRpm(), 
		         mtr->getDescription()->maxRpm - 10.0f, 
				 mtr->getDescription()->maxRpm + 10.0f ) );

	BOOST_CHECK( mtr->getTorque() <= mtr->getDescription()->torqueCurve.getValue( mtr->getDescription()->maxRpm ) );
	mtr->deccelerate( 10.0f );
	BOOST_CHECK( mtr->getRpm() <= mtr->getDescription()->minRpm );
	BOOST_CHECK( mtr->getTorque() <= mtr->getDescription()->torqueCurve.getValue( mtr->getDescription()->minRpm ) );
	std::cout << "********** Combustion motor test end **********" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()