#include "module.h"

BOOST_AUTO_TEST_SUITE( GearBoxTest )

BOOST_AUTO_TEST_CASE( Continuous )
{
	std::cout << "********** Continuous gearbox test begin **********" << std::endl;
	std::auto_ptr<Zmvs::VehicleContinuousGearBoxDsc> cgb_dsc( new Zmvs::VehicleContinuousGearBoxDsc );
	BOOST_CHECK( !cgb_dsc->isValid() );
	cgb_dsc->defaultConfig();
	BOOST_CHECK( cgb_dsc->isValid() );

	std::auto_ptr<Zmvs::VehicleContinuousGearBox> cgb( 
		static_cast< Zmvs::VehicleContinuousGearBox* >( 
		Zmvs::SDKFactory::getSingleton()->createGearBox( cgb_dsc.get() ) ) );

	BOOST_CHECK( cgb->getDescription()->motorRpm == cgb_dsc->motorRpm );
	BOOST_CHECK( cgb->getRatio() == cgb->getDescription()->maxRatio );
	cgb->up( 1000.0f );
	BOOST_CHECK( cgb->getRatio() <= cgb->getDescription()->maxRatio );
	cgb->down( 1000.0f );
	BOOST_CHECK( cgb->getRatio() >= cgb->getDescription()->minRatio );
	std::cout << "********** Continuous gearbox test end **********" << std::endl;
}

BOOST_AUTO_TEST_CASE( Discrete )
{
	std::cout << "********** Discrete gearbox test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleDiscreteGearBoxDsc > cgb_dsc( new Zmvs::VehicleDiscreteGearBoxDsc );
	BOOST_CHECK( !cgb_dsc->isValid() );
	cgb_dsc->defaultConfig();
	BOOST_CHECK( cgb_dsc->isValid() );

	std::auto_ptr< Zmvs::VehicleDiscreteGearBox > cgb( static_cast< Zmvs::VehicleDiscreteGearBox* >( Zmvs::SDKFactory::getSingleton()->createGearBox(cgb_dsc.get() ) ) );
	BOOST_CHECK( cgb->getDescription()->gearsRatio == cgb_dsc->gearsRatio );
	BOOST_CHECK( cgb->getDescription()->switchDelai == cgb_dsc->switchDelai );
	for( size_t  i = 0; i < 10; i++ )
	{
		cgb->up( 1000.0f );
		BOOST_CHECK( cgb->getRatio() == cgb->getDescription()->gearsRatio.at( cgb->getGear() ) );
	}
	for( size_t  i = 0; i < 10; i++ )
	{
		cgb->down( 1000.0f );
		BOOST_CHECK( cgb->getRatio() == cgb->getDescription()->gearsRatio.at( cgb->getGear() ) );
	}

	size_t prevGear = cgb->getGear();
	cgb->up( Zmvs::VehicleDiscreteGearBoxDsc::DEFAULT_GEAR_SWITCH_DELAI / 2.0f );
    BOOST_CHECK( prevGear == cgb->getGear() );
	
	cgb->setGear( 1 );
	BOOST_CHECK( cgb->getGear() == 1 );
	cgb->setGear( 100 );
	BOOST_CHECK( cgb->getGear() == 1 );
	BOOST_CHECK( cgb->getRatio( 100 ) == Zmvs::VehicleDiscreteGearBox::GEAR_OUT_OF_RANGE );
	BOOST_CHECK( !cgb->isLocked() );
	cgb->getDescription()->lock = true;
	cgb->setGear( 2 );
	BOOST_CHECK( cgb->getGear() == 1 );
	BOOST_CHECK( cgb->isLocked() );
	std::cout << "********** Discrete gearbox test end **********" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()