#include "module.h"

BOOST_AUTO_TEST_SUITE( motion )

BOOST_AUTO_TEST_CASE( Manual )
{
	std::cout << "********** Manual gearbox motion test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleManualGearBoxMotionDsc > mgbm_dsc( new Zmvs::VehicleManualGearBoxMotionDsc );
	BOOST_CHECK( !mgbm_dsc->isValid() );
	mgbm_dsc->defaultConfig();
	BOOST_CHECK( mgbm_dsc->isValid() );
	//std::cout << *( mgbm_dsc.get() ) << std::endl;
	// TO DO debug output

	std::auto_ptr< Zmvs::VehicleManualGearBoxMotion > mgbm( static_cast< Zmvs::VehicleManualGearBoxMotion* >( Zmvs::SDKFactory::getSingleton()->createGearBoxMotion( mgbm_dsc.get() ) ) );
	BOOST_CHECK( mgbm->getDescription()->isValid() );
	mgbm->gearNeutral();
	mgbm->update( 10.0f );
	BOOST_CHECK( mgbm->getGearBox()->getGear() == Zmvs::VehicleDiscreteGearBox::NEUTRAL );
	mgbm->gearUp();
	mgbm->update( 10.0f );

	BOOST_CHECK( mgbm->getGearBox()->getGear() == Zmvs::VehicleDiscreteGearBox::FIRST );
	
	for(int i = 0; i < 10; i++ )
	{
		Zmvs::Real prevTorque = mgbm->getTorque();
		Zmvs::Real prevRotation = mgbm->getRotation();
	    mgbm->accelerate( 1.0f );
		BOOST_CHECK( !Zmvs::Math::isZero( mgbm->getTorque() ) );
		BOOST_CHECK( mgbm->getRotation() >= prevRotation );
		mgbm->update( 1.5f );
		mgbm->gearUp();
	}
	std::cout << "gear box = " << mgbm->getGearBox()->getGear() << std::endl;
	std::cout << "gear box max gear = " << mgbm->getGearBox()->getMaxGear() << std::endl;
	BOOST_CHECK( mgbm->getGearBox()->getGear() == mgbm->getGearBox()->getMaxGear() );
	std::cout << "********** Manual gearbox motion test end **********" << std::endl;
}

BOOST_AUTO_TEST_CASE( Auto )
{
	std::cout << "********** Auto gearbox motion test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleAutoGearBoxMotionDsc > agbm_dsc( new Zmvs::VehicleAutoGearBoxMotionDsc );
	BOOST_CHECK( !agbm_dsc->isValid() );
	agbm_dsc->defaultConfig();
	BOOST_CHECK( agbm_dsc->isValid() );

	std::auto_ptr< Zmvs::VehicleAutoGearBoxMotion > agbm( static_cast< Zmvs::VehicleAutoGearBoxMotion* >( Zmvs::SDKFactory::getSingleton()->createGearBoxMotion( agbm_dsc.get() ) ) );
	BOOST_CHECK( agbm->getDescription()->isValid() );

	BOOST_CHECK( agbm->getGearBox()->getGear() == Zmvs::VehicleDiscreteGearBox::NEUTRAL );
	std::cout << "current auto gearbox state = " << agbm->getGear() << std::endl;
	BOOST_CHECK( agbm->getGear() == Zmvs::VehicleAutoGearBoxMotion::Auto_GearBox_Neutral );
	agbm->update( 2.0f );
	//agbm->update( 1.0f );
	std::cout << "rpm after update = " << agbm->getMotor()->getRpm() << std::endl;
	BOOST_CHECK( Zmvs::Math::clamp( agbm->getMotor()->getRpm(),
		         agbm->getMotor()->getDescription()->minRpmToGearDown - 10.0f,
                 agbm->getMotor()->getDescription()->minRpmToGearDown + 10.0f ) );

	Zmvs::Real prevRpm = agbm->getMotor()->getRpm();
	agbm->accelerate( 1.0f );
	std::cout << "prevRpm = " << prevRpm << std::endl;
	std::cout << agbm->getMotor()->getRpm() << std::endl;
	BOOST_CHECK( agbm->getMotor()->getRpm() > prevRpm );
	agbm->update( 1.0f );
    
	std::cout << "********** Auto gearbox motion test end **********" << std::endl;
}

BOOST_AUTO_TEST_CASE( SpecialAuto )
{
	std::cout << "********** Special auto gearbox motion test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleSpecialAutoGearBoxMotionDsc > sagbm_dsc( new Zmvs::VehicleSpecialAutoGearBoxMotionDsc );
	BOOST_CHECK( !sagbm_dsc->isValid() );
	sagbm_dsc->defaultConfig();
	BOOST_CHECK( sagbm_dsc->isValid() );

	std::auto_ptr< Zmvs::VehicleSpecialAutoGearBoxMotion > sagbm( static_cast< Zmvs::VehicleSpecialAutoGearBoxMotion* >( Zmvs::SDKFactory::getSingleton()->createGearBoxMotion( sagbm_dsc.get() ) ) );
	BOOST_CHECK( sagbm->getDescription()->isValid() );
    std::cout << "********** Special auto gearbox motion test end **********" << std::endl;
}

BOOST_AUTO_TEST_CASE( Continuous )
{
	std::cout << "********** Continuous gearbox motion test begin **********" << std::endl;
	std::auto_ptr< Zmvs::VehicleContinuousGearBoxMotionDsc > cgbm_dsc( new Zmvs::VehicleContinuousGearBoxMotionDsc );
	BOOST_CHECK( !cgbm_dsc->isValid() );
	cgbm_dsc->defaultConfig();
	BOOST_CHECK( cgbm_dsc->isValid() );

	std::auto_ptr< Zmvs::VehicleContinuousGearBoxMotion > cgbm( static_cast< Zmvs::VehicleContinuousGearBoxMotion* >( Zmvs::SDKFactory::getSingleton()->createGearBoxMotion( cgbm_dsc.get() ) ) );
	BOOST_CHECK( cgbm->getDescription()->isValid() );
	BOOST_CHECK( cgbm->getGearBox()->getState() == Zmvs::VehicleContinuousGearBox::VEHICLE_CONTINUOUS_GEAR_BOX_NEUTRAL );
	cgbm->update( 1.0f ); // TO DO bug when time > 1.0 second
	cgbm->update( 1.0f );
	BOOST_CHECK( cgbm->getMotor()->getRpm() >= cgbm->getMotor()->getDescription()->minRpm );
	Zmvs::Real prevRpm = cgbm->getMotor()->getRpm();
	cgbm->accelerate( 1.0f );
	std::cout << "curr Rpm > prev Rpm : " << cgbm->getMotor()->getRpm() << " " << prevRpm << std::endl;
	BOOST_CHECK( cgbm->getMotor()->getRpm() > prevRpm );

	std::cout << "********** Continuous gearbox motion test end **********" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()